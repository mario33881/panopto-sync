import math
import re
import time
import io
from threading import Thread, Event
from typing import Mapping, Optional

import progress.bar

from logs import Log, logger, ansi, hide_cursor, show_cursor
from core import criticalsection, nice_time, nice_bytes, coalesce, anon_object


# monkey patching progress library to conform colorama philosophy
def hide_cur_wrap(self):  # noqa
    hide_cursor()
    return ""


def show_cur_wrap(self):  # noqa
    show_cursor()
    return ""


progress.HIDE_CURSOR = anon_object(__repr__=hide_cur_wrap)
progress.SHOW_CURSOR = anon_object(__repr__=show_cur_wrap)


RESET_LINE = "\r" + ansi.clear_line(2)  # reset horizontal, clear all


class Animated:
    def is_anim(self):
        pass

    def start_anim(self):
        pass

    def next_anim(self):
        pass

    def stop_anim(self):
        pass


class Animator:
    def __init__(self, anim: Animated, tick=0.035):
        self.anim = anim
        self.tick = tick
        self.thread: Optional[Thread] = None
        self.stop_signal = Event()

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.stop()

    def start(self):
        self.anim.start_anim()
        self.stop_signal.clear()
        self.thread = Thread(target=self.__parallel, daemon=True)
        self.thread.start()

    def __parallel(self):
        while not self.stop_signal.is_set():
            self.anim.next_anim()
            time.sleep(self.tick)

    def stop(self):
        self.stop_signal.set()
        while self.thread.is_alive():
            time.sleep(self.tick)
        self.anim.stop_anim()


# https://github.com/verigak/progress/blob/master/progress/__init__.py
class CustomMixedBar(progress.Progress, Animated):
    RGX_PERCENT = re.compile(r"0*([0-9]+?(?:\.[0-9]+)?)%")
    SHADES = progress.bar.ShadyBar.phases
    MAX_SHADE = SHADES[-1]
    SPEED_WND_SEC = 5

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not kwargs.get("message2"):
            self.message2 = None
        self.suffix = "%(percent)6.2f%% %(elapsed_t)s"
        self.width = 24
        self.max = 100
        self.shade_width = 10
        self.shade_index = 0
        self.force_vague = False
        self.curr_dwnbytes = None
        self.last_dwnbytes = 0
        self.last_speed = 0
        self.history_speed = []
        self.animator = Animator(self)
        self._render = io.StringIO()
        self._animating = False

    @property
    def elapsed(self):
        return time.monotonic() - self.start_ts

    @property
    def elapsed_t(self):
        return nice_time(self.elapsed, display=True)

    @property
    def eta(self):
        return nice_time(super().eta, display=True)

    @property
    def speed(self):
        if self.curr_dwnbytes is None:
            return self.last_speed
        elapsed = self.elapsed

        diff = self.curr_dwnbytes - self.last_dwnbytes
        self.last_dwnbytes = self.curr_dwnbytes
        inside_time_window = lambda t: elapsed - t[0] <= self.SPEED_WND_SEC
        self.history_speed = list(filter(inside_time_window, self.history_speed))
        self.history_speed.append((elapsed, diff))

        self.last_speed = max(0, sum(map(lambda t: t[1], self.history_speed))) // self.SPEED_WND_SEC
        return self.last_speed

    @staticmethod
    def make_msg(text: str) -> str:
        pad = Log.get_pad(logger.level - 1, logger.delta)
        return pad + " " + text.rjust(13)

    def is_anim(self):
        return self._animating

    def start_anim(self):
        self._animating = True

    def next_anim(self):
        self.shade_index += 1 if self.force_vague or self.index == 0 else -1
        self.__update()

    def stop_anim(self):
        self._animating = False

    def start(self):
        self.animator.start()

    def finish(self):
        self.animator.stop()
        self.__update()
        super().finish()
        # self.clearLine()

    def update(self):
        if not self._animating:
            self.__update()

    def __update(self):
        self._render.seek(0)
        self._render.write(" " * self.width)

        if self._animating or self.force_vague:
            width_mix = self.width + self.shade_width
            iwrap = width_mix + (self.shade_width // 2)
            self.shade_index %= iwrap
            # handle right-to-left motion
            if self.shade_index < 0:
                self.shade_index = iwrap - 1
            lens = len(CustomMixedBar.SHADES) - 1
            istart_cap = min(self.shade_index, width_mix - 1)
            istart = istart_cap - self.shade_width
            istop = min(istart_cap, self.width)

            self._render.seek(max(istart, 0))
            for i in range(istart, istop):
                if i < 0:
                    continue
                x = (i - istart) / self.shade_width
                y = math.sin(x * math.pi)
                y = round(y * lens)
                self._render.write(CustomMixedBar.SHADES[y])

        if not self.force_vague:
            completed = int(self.index / self.max * self.width)
            self._render.seek(0)
            for i in range(0, completed):
                self._render.write(self.MAX_SHADE)

        print(RESET_LINE, self.message, self._render.getvalue(), self.suffix % self, end="", file=self.file)
        self.file.flush()

    @criticalsection
    def progress_hook(self, data: Mapping):
        # errlog(data)
        st = data["status"]
        total = ""
        if st in ("downloading", "finished"):
            # elapsed = data.get('elapsed')
            # if elapsed is not None:
            #    elapsed = ' '+nice_time(elapsed, display=True)
            # elapsed = elapsed or ''

            # eta = data.get('eta')
            # if eta is not None:
            #    eta = f' ~{nice_time(eta, display=True)}'
            # eta = eta or ''
            eta = " " + self.eta

            totbytes = coalesce(data, "total_bytes")
            estimate = False
            if totbytes is None:
                estimate = True
                totbytes = coalesce(data, "total_bytes_estimate")
            if totbytes is not None:
                pre = "~" if estimate else ""
                total = f" {pre}{nice_bytes(totbytes)}"
            self.curr_dwnbytes = coalesce(data, "downloaded_bytes")

            # (force internal speed calculation)
            # speed = data.get("speed")
            # if speed is not None:
            #     speed = f"{nice_bytes(speed)}/s"
            # else:
            #     speed = self.speed or ""
            speed = self.speed
            speed = f" {nice_bytes(speed)}/s" if speed else ""
            self.curr_dwnbytes = None  # prevent speed recalculation

            self.suffix = "%(percent)6.2f%% %(elapsed_t)s{}{}{}".format(eta, total, speed)
            if totbytes:
                self.max = totbytes
                self.goto(coalesce(data, "downloaded_bytes", default=self.index))
            else:
                self.max = coalesce(data, "fragment_count", "segment_count")
                self.goto(coalesce(data, "fragment_index", "handled_count", default=self.index))

        if st == "finished":
            if self.message2 is not None:
                self.message = self.message2
            self.suffix = "%(percent)6.2f%% %(elapsed_t)s{}".format(total)
            self.goto(self.max)
            self.force_vague = True

    def progress_hook_finished(self):
        self.force_vague = False
        # self.clearLine()

    def steps_mode_init(self, total):
        self.max = total
        self.suffix = "%(index)d/%(max)d %(elapsed_t)s ~%(eta)s %(avg).1f/s"
        self.update()

    def steps_hook(self):
        self.next()

    def mkvmerge_progress(self, outs: str) -> Optional[int]:
        last = None
        for m in CustomMixedBar.RGX_PERCENT.finditer(outs):
            self.goto(float(m[1]))
            last = m
        return last.end() if last is not None else None
