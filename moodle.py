import re
import uuid
import urllib.parse as ulp
from dataclasses import dataclass
from itertools import chain
from email.utils import parsedate_to_datetime
from typing import List, Optional, Callable

from requests import Response

from logs import Verb, logger, log, errlog
from core import digest, group_by, makesure, take_unique_with_order
from web import StaticUrls, Platform, AuthProcedure, AbstractSSOAuth, HTMLIter, HTMLNode
from content import Course, Lesson, Stream, RawEntry

# from title_format import set_course_name_fields


class MoodleUnsubscribedError(Exception):
    pass


@dataclass
class MoodleAuth(AuthProcedure):
    domain: str
    sso_auth: AbstractSSOAuth
    LABEL = "Moodle"
    TEST_PATH = "/auth/shibboleth/index.php"  # "/login/index.php"
    PREFS_PATH = "/user/preferences.php"  # just more slim to load
    # AUTH_PATH = "/Shibboleth.sso/SAML2/POST"  (handled via SAMLv2)

    RGX_SESSKEY_USERID = re.compile(
        r"sesskey[\"']?[:= ]+?([\"']?)\b(?P<sesskey>[^\"']+?)\b\1.+"
        r"userid[\"']?[:= ]+?([\"']?)\b(?P<userid>[0-9]+?)\b\3",
        flags=re.I | re.M | re.S,
    )

    def __post_init__(self):
        self.userid: str = ""
        self.sesskey: str = ""

    def _get_session(self):
        with self.session.get(self.urljoin(self.PREFS_PATH)) as response:
            head = response.text[:21000]
            match = self.RGX_SESSKEY_USERID.search(head)
            self.sesskey, self.userid = match["sesskey"], match["userid"]
            errlog("userid:", self.userid, verbos=Verb.auth)
            errlog("sesskey:", self.sesskey, verbos=Verb.auth)

    def authorize(self, redir: str = None) -> bool:
        self.__post_init__()
        if not redir:
            chk, redir = self.is_authorized()
            if chk:
                self._get_session()
                return True
        redir = self.sso_auth.login_portal(redir)
        rparts = ulp.urlparse(redir)
        assert self.domain in rparts.netloc and rparts.path == self.TEST_PATH
        ret = self.is_authorized()[0]
        if ret:
            self._get_session()
        return ret


class Moodle(Platform["Moodle.Urls"]):
    class Urls(StaticUrls):
        COURSES = "{}/lib/ajax/service.php"
        COURSE_PAGE_FMT = "{}/course/view.php?id="
        RESOURCE_VIEW = "{}/mod/resource/view.php"  # id=
        URL_VIEW = "{}/mod/url/view.php"  # id=
        PANOPTO_BLOCK = "{}/blocks/panopto/panopto_content.php"  # univr-specific
        RGX_MP4: re.Pattern

        def __init__(self, domain: str):
            super().__init__(domain)
            self.COURSE_PAGE_FMT += "{}"
            self.RGX_MP4 = re.compile(
                rf"https://{re.escape(domain)}/pluginfile\.php"
                r"/[0-9]+/mod_resource/content/[0-9]+/([^?&/]+?)\.mp4"
            )

    auth: MoodleAuth
    API_LIST_COURSES = "core_course_get_recent_courses"
    RGX_MPEG_ICO = re.compile(r"\b.*?mpeg-[0-9]+?\b")
    RGX_NAME_NO_VER = re.compile(r"^(.+?) *#?[0-9.]+\xB0?$")

    def __post_init__(self):
        super().__post_init__()
        self.auth = MoodleAuth(session=self.session, domain=self.domain, sso_auth=self.sso_auth)

    def probe_moodle_url_view(self, url) -> Optional[str]:
        # https://github.com/moodle/moodle/blob/master/mod/url/locallib.php#L279
        with self.session.get(url, no_errs=False, strict200=False) as response:
            link = None
            errlog("moodle url:", url, verbos=Verb.scraping)
            if response.is_redirect:
                link = response.next.url
                errlog("ok:", link, verbos=Verb.scraping)

            elif response.status_code == 200:  # get link from page
                errlog("200 - sub page", verbos=Verb.scraping)
                try:
                    parent = next(HTMLIter(response.text).search(None, _class='urlworkaround'))
                    link = next(parent.search1('a', href=None))['href']
                    errlog("ok:", link, verbos=Verb.scraping)
                except StopIteration:
                    pass
            # errlog(f'{resp.status}: {link}')

        return link

    def probe_moodle_resource_view_embed_mp4(self, url) -> Optional[re.Match]:
        with self.session.get(url, no_errs=False) as response:
            if response.status_code != 200:
                return None
            content_type = response.headers["content-type"]
            # errlog(content_type)
            if not content_type.startswith("text/html"):
                return None

            linkmatch = self.urls.RGX_MP4.search(response.text)

        # errlog('almost there')
        return linkmatch

    def get_all_courses(self) -> List[Course]:
        """Finds all courses by scraping Moodle main page, and by reversed requests."""
        # https://github.com/moodle/moodle/blob/master/lib/db/services.php#L660=

        with self.session.post(
            self.urls.COURSES,
            params=dict(sesskey=self.auth.sesskey, info=self.API_LIST_COURSES),
            json=[
                dict(
                    index=0,
                    methodname=self.API_LIST_COURSES,
                    args=dict(userid=self.auth.userid, limit=1000),
                )
            ],
        ) as response:
            info = response.json()[0]

        iserror = info.get("error")
        # errlog("info:", data, verbos=Verb.scraping)
        if iserror:
            exc = info["exception"]
            # errlog("Moodle reports an error:")
            # with logger:
            # errlog(exc["message"], pointer=True)
            errcode = exc["errorcode"]
            if errcode == "invalidsesskey":
                raise PermissionError(iserror)
            raise RuntimeError(iserror)
        courses = info.get("data")
        if not courses:
            errlog(info)
            raise RuntimeError("Data does not contain courses!")

        # ALERT entries contain "summary" and "courseimage", two very big strings

        ret = []
        courses = sorted(courses, key=lambda co: (co["hidden"], co["isfavourite"], co["timeaccess"]))
        for c in courses:
            crs = Course(
                name=c["fullname"],
                id=str(c["id"]),
            )
            ret.append(crs)
        return ret
        # errlog("found:", len(courses), verbos=Verb.scraping)
        # for c in courses:
        #     anchor = c.find_elements_by_css_selector("a.coursename")[0]
        #     link = anchor.get_attribute("href")
        #     name = anchor.find_elements_by_css_selector('span.multiline')[0].get_attribute("innerHTML")
        #     m = self.RGX_YEAR.match(name)
        #     year = int(m[3]) if m else None
        #     crs = Course(
        #         name=name,
        #         id=self.RGX_ID.findall(link)[0],
        #         year=year,
        #         lessons={},
        #     )
        #     ret.append(crs)
        # ret = set(ret)
        # errlog("found:", ret, verbos=Verb.scraping)
        # return ret

    @staticmethod
    def check_course_subscription(page: Response):
        if page.is_redirect:
            makesure(lambda: '/enrol/' in page.next.url, "Unexpected redirect.")
            raise MoodleUnsubscribedError()

    def get_links_from_course_id(
        self,
        course_id,
        checker: Callable[[str], bool],
        mapper: Callable[[str], str] = None,
    ) -> List[str]:
        """Finds all specific links by scraping a specific Moodle course page."""
        ret = []
        discover = []
        log()

        def stats():
            log(end="", erase_prev=True)
            l = len(ret)
            num = ""
            if l > 0:
                num = f" ({l})"
            log("Searching for lessons...", num, sep="")

        stats()

        with self.session.get(self.urls.COURSE_PAGE_FMT.format(course_id), strict200=False) as page:
            self.check_course_subscription(page)

            # get all possible anchors
            errlog("[searching anchors]", verbos=Verb.scraping)
            with logger:
                for a in HTMLIter(page.text).search('a', href=None):
                    link = a["href"]
                    errlog(link, verbos=Verb.scraping)
                    if checker(link):
                        lid = mapper(link) if mapper else link
                        if not lid:
                            continue
                        ret.append(lid)
                        stats()
                    elif link.startswith(self.urls.URL_VIEW):
                        with logger:
                            errlog("is moodle url view", verbos=Verb.scraping)
                        discover.append(link)

        # explore moodle urls
        if discover:
            errlog(verbos=Verb.scraping)
            errlog("[exploring moodle urls]", verbos=Verb.scraping)
        with logger:
            for url in discover:
                # errlog(url)
                link = self.probe_moodle_url_view(url)
                if link is not None and checker(link):
                    lid = mapper(link) if mapper else link
                    if lid:
                        ret.append(lid)
                        stats()
                    errlog(lid, verbos=Verb.scraping)

        # take unique
        ret = take_unique_with_order(ret)
        log(end="", erase_prev=True)
        return ret

    def __discover_raw_lessons(self, resp: Response) -> List[RawEntry]:
        found = []

        # get content page blocks
        for cont in HTMLIter(resp.text).search(None, _class='content'):
            # get block header text
            h3 = next(cont.search1("h3"), None)
            h3name = h3.text if h3 else ""
            # errlog("h3:", h3name)
            # get labels and links
            lastlabel = ""
            i1 = cont.search(None, _class='label')
            i2 = (next(sect.search('a')) for sect in cont.search(None, _class='resource'))
            for e in chain(i1, i2):
                e: HTMLNode
                # errlog("tag:", tag)
                if e.tag == "li":  # label
                    lastlabel = e.text.strip().strip("\n")

                elif e.tag == "a":  # link
                    link = e["href"]
                    if not link.startswith(self.urls.RESOURCE_VIEW):
                        # errlog('not resource:', link)
                        continue
                    # errlog("link:", link)
                    img = next(e.search1("img"), None)
                    if not img:
                        # errlog('no sub images:', link)
                        continue
                    if not self.RGX_MPEG_ICO.findall(img["src"]):
                        # errlog('no sub mpeg images:', link)
                        continue
                    # errlog('found:', link)
                    title = e.text.split("\n")[0]
                    # errlog("title:", title)
                    entry = RawEntry(url=link, title=title, label=lastlabel, header=h3name)
                    found.append(entry)

        return found

    def get_all_raw_lessons_data(self, course_id) -> List[Lesson]:
        ret = []
        log()

        def stats():
            log(end="", erase_prev=True)
            l = len(ret)
            num = ""
            if l > 0:
                num = f" ({l})"
            log("Searching for raw lessons...", num, sep="")

        stats()

        with self.session.get(self.urls.COURSE_PAGE_FMT.format(course_id)) as page:
            self.check_course_subscription(page)
            found = self.__discover_raw_lessons(page)
            found = self.__assign_raw_lessons_names(found)

            # explore moodle urls, take unique
            for entry in found:
                m = self.probe_moodle_resource_view_embed_mp4(entry.url)
                if m is None: continue
                url = m[0]  # fixURL(m[0])
                errlog("raw url:", url, verbos=Verb.scraping)

                # split at protocol commas ("https://...") and requote url
                # commas = url.find(":") + 1
                # url = url[:commas] + quote(url[commas:])
                with self.session.head(url) as info:
                    last_modified = info.headers["last-modified"]
                # errlog(last_modified)
                luuid = url.encode()
                luuid = bytes.fromhex(digest(luuid, digest_size=16))
                luuid = str(uuid.UUID(bytes=luuid))
                dtparse = lambda s: parsedate_to_datetime(s).astimezone(None).replace(tzinfo=None)
                nn = lambda v, f: f(v) if v is not None else None
                lesson = Lesson(
                    id=luuid,
                    is_raw=True,
                    name=entry.title,  # m[1],
                    date=nn(last_modified, dtparse),
                    streams=[
                        Stream(
                            url=url,  # NEEDS cookies
                            tag="RAW",
                            segments=[],
                        )
                    ],
                )
                ret.append(lesson)
                stats()
        log(end="", erase_prev=True)
        return ret

    @staticmethod
    def __assign_raw_lessons_names(found: List[RawEntry]) -> List[RawEntry]:
        headerkey = lambda en: en.header
        labelkey = lambda en: en.label
        dummykey = lambda en: ""
        prefixes = set()

        def group_equal(g2: List, f: Callable, r: Callable):
            nonlocal prefixes
            name_nover = None
            for e2 in g2:
                if f(e2) != "":
                    return  # lessons with different label
                m2 = Moodle.RGX_NAME_NO_VER.match(e2.title)
                if not m2:
                    return  # lessons with different title
                prefix2 = m2[1]
                prefixes.add(prefix2)
                if name_nover is None:
                    name_nover = prefix2
                elif prefix2 != name_nover:
                    return  # lessons with different title
            # lessons with very similar title
            for e2 in g2:
                e2.title = r(e2)

        # assumptions:
        #    - in case of multiple headers, lessons must have different titles
        #    - grouping by header, lessons must be under the same label
        for _1, g in group_by(found, headerkey):
            g = list(g)
            if len(g) < 2:
                e = g[0]
                e.title = max((e.title, e.header), key=len)
                continue
            group_equal(g, labelkey, headerkey)

        aslist = lambda t: (t[0], list(t[1]))
        labelgroups = list(map(aslist, group_by(found, labelkey)))
        for _1, g in labelgroups:
            if len(g) < 2:
                continue
            group_equal(g, dummykey, labelkey)

        for _1, g in labelgroups:
            e = g[0]
            # errlog(len(g), e.title, e.label)
            if len(g) != 1:
                continue
            m = Moodle.RGX_NAME_NO_VER.match(e.title)
            if not m:
                continue
            prefix = m[1]
            # errlog(prefix, prefixes)
            if prefix in prefixes:
                e.title = e.label

        return found
