import re
from typing import Tuple, Dict, Optional, Any

from core import format_default, linear_maps, windows_compatible_path


RGX_YEAR = re.compile(r"^(.+?) *(\(([0-9]+)[/\\\-]+([0-9]+)\)) *$")  # Course


def extract_course_year(name: str) -> Tuple[Optional[int], str]:
    m = RGX_YEAR.match(name)
    year = int(m[3]) if m else None
    name_clean = m[1] if m else name
    return year, name_clean


# https://stackoverflow.com/a/48557664
ROMAN_NUMERALS = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
RGX_NAME_ROMAN = re.compile(r"[(\[{]([MDCLXVI]+)[)\]}]")


def from_roman(num: str):
    num = num.upper()
    result = 0
    for i, c in enumerate(num):
        # if (last digit) or (digit >= next digit) # VI=6
        if (i + 1) == len(num) or ROMAN_NUMERALS[c] >= ROMAN_NUMERALS[num[i + 1]]:
            result += ROMAN_NUMERALS[c]
        else:
            result -= ROMAN_NUMERALS[c]
    return result


def convert_romans(text: str):
    return RGX_NAME_ROMAN.sub(lambda r: "#" + str(from_roman(r[1])), text)


def remove_romans(text: str):
    return RGX_NAME_ROMAN.sub(lambda r: "", text)


RGX_NAME_NUM = re.compile(r"(\s)n\.\s+([0-9])")


def compact_text_index(text: str):
    return RGX_NAME_NUM.sub(r"\g<1>n.\g<2>", text)


RGX_INV_WORDS_T1 = re.compile(r" *(?:lezione(?: +del| +di)?|lesson(?: +of)?) *$| +(?:del|di|of)  +|  +", re.IGNORECASE)
RGX_INV_PARENT_T1 = re.compile(r" *[(\[{] *$")
RGX_INV_PARENT_T2 = re.compile(r"^ *[)\]}] *")
RGX_INV_PUNCT_T1 = re.compile(r" *[.,;:#\\/\-_] *$")
RGX_INV_PUNCT_T2 = re.compile(r"^ *[.,;:#\\/\-_] *")
RGX_INV_PUNCT_G = re.compile(r"^[.,;:\\/\-_ ]*|^ +[.,;:#\\/\-_ ]*|[.,;:#\\/\-_] *$")


def clean_invalid_name(t1: str, t2: str = None) -> str:
    def strip1(r):
        nonlocal t1
        b = r.search(t1)
        m = b is not None
        if m:
            t1 = t1[: b.start()]
        return m

    def strip2(r):
        nonlocal t2
        b = r.search(t2)
        m = b is not None
        if m:
            t2 = t2[b.end() :]
        return m

    if t2 is not None:
        strip1(RGX_INV_WORDS_T1)
        while True:
            b1 = RGX_INV_PARENT_T1.search(t1)
            b2 = RGX_INV_PARENT_T2.search(t2)
            if b1 and b2:
                t1 = t1[: b1.start()]
                t2 = t2[b2.end() :]
            else:
                break
        if not strip1(RGX_INV_PUNCT_T1):
            strip2(RGX_INV_PUNCT_T2)
        return (t1 + t2).strip()
    """
    b = RGX_INV_PUNCT_T1.search(t1)
    if b is not None:
        t1 = t1[:b.start()]
    else:
        b = RGX_INV_PUNCT_T2.search(t1)
        if b is not None:
            t1 = t1[b.end():]
    return t1.strip()
    """
    return RGX_INV_PUNCT_G.sub("", t1)


RGX_CAMEL_MIXED = re.compile(r"([A-Z])(?=[A-Z][a-z]{2})|(\S[a-z])(?=[A-Z0-9])")


def add_spaces_camel_case(txt: str) -> str:
    return RGX_CAMEL_MIXED.sub(r"\g<1>\g<2> ", txt)


pathify_lesson_name = windows_compatible_path


def pathify_course_name(c: str) -> str:
    m = RGX_YEAR.match(c)
    if m:
        c = " ".join([m[2], m[1]])
    c = windows_compatible_path(c)
    return c


ORIGINAL_FILENAME_FMT = "{name}"
BETTER_FILENAME_FMT = "{name_fixed}"
DATE_FILENAME_FMT = "{date:%Y-%m-%d (%H.%M)}"


def format_lesson_filename(title_format, course_name: str, lesson_data: Dict[str, Any]) -> str:
    txt = format_default(title_format, lesson_data).replace("_", " ")
    if txt.lower() == course_name.lower():
        txt = ""
    else:
        # rfmt = fr"(?:\bdi +)?{re.escape(course.name_clean)}"
        # re_course = re.compile(rfmt, re.IGNORECASE)
        # errlog(course.name, rfmt)
        # txt = re.sub(re_course, "", txt)
        txt = linear_maps(
            txt,
            # convert_romans,
            remove_romans,
            compact_text_index,
            clean_invalid_name,
            add_spaces_camel_case,
            pathify_lesson_name,
        )
    if not txt:
        txt = format_default(DATE_FILENAME_FMT, lesson_data)
    else:
        txt = txt[0].capitalize() + txt[1:]
    return txt


def deduplicate_names(filename: str, group_index: int) -> str:
    return filename + f" #{group_index}"  # format_default(Synchronizer.DUPLICATES_ADD_FMT, dict(lesson))
