import argparse
import re
from pathlib import Path
from typing import List, Union, Protocol

from logs import Verb, errlog
from core import makenames


@makenames
class ArgumentsType(Protocol):
    id_path: Union[str, Path]
    cookies_path: Union[str, Path]
    sync_dir: Union[str, Path]
    verb_flags: List[Verb]
    force_older: bool
    force_existing: bool
    no_search: bool
    dry_run: bool
    known_courses: bool
    unknown_courses: bool
    courses_filters: List[re.Pattern]
    force_meta: bool
    only_meta: bool
    only_rename: bool


ACT_GENID = "genid"
ACT_SHOWID = "showid"
ACT_SYNC = "sync"


def make_command_parser():
    _mfc = lambda prog: argparse.RawTextHelpFormatter(prog, max_help_position=29)

    parser = argparse.ArgumentParser(  # noqa
        description="""\
Automatically downloads and synchronizes UniVR Panopto lessons.
By Microeinstein

To show additional arguments, specify an action (eg. sync -h)""",
        formatter_class=_mfc,
    )
    subparsers = parser.add_subparsers(metavar="ACTION", dest="action", prog=None, required=True)  # noqa

    parser.add_argument(
        "--idfile",
        "-i",
        metavar="path",
        dest=ArgumentsType.K_ID_PATH,
        default="default.id",
        help="credentials file",
    )
    def flags(names):
        return [Verb[n] if n in Verb.__members__ else (
            errlog("Known flags:", ', '.join(Verb.__members__.keys()), "\n"),
            parser.error(f"{n} is unknown.")
        ) for n in names.split(',')]
    parser.add_argument(
        "--verbose",
        "-v",
        dest=ArgumentsType.K_VERB_FLAGS,
        type=flags,
        default=[],
        help="set verbosity flags (comma separated)",
    )

    subparsers.add_parser(ACT_GENID, formatter_class=_mfc, help="create credentials file")
    subparsers.add_parser(ACT_SHOWID, formatter_class=_mfc, help="show credentials file content")
    snc = subparsers.add_parser(ACT_SYNC, formatter_class=_mfc, help="synchronize the lessons")

    snc.add_argument(
        "--cookies",
        "-c",
        metavar="path",
        dest=ArgumentsType.K_COOKIES_PATH,
        default="default.cookies",
        help="browser cookies database file",
    )
    snc.add_argument(
        "--syncdir",
        "-d",
        metavar="path",
        dest=ArgumentsType.K_SYNC_DIR,
        default="lessons",
        help="synchronization folder",
    )
    snc.add_argument(
        "--force-older",
        "-w",
        dest=ArgumentsType.K_FORCE_OLDER,
        action="store_true",
        help="do not abort if using an older download algorithm",
    )
    snc.add_argument(
        "--force-existing",
        "-e",
        dest=ArgumentsType.K_FORCE_EXISTING,
        action="store_true",
        help="download a lesson even if a video already exists",
    )
    snc.add_argument(
        "--no-search",
        "-n",
        dest=ArgumentsType.K_NO_SEARCH,
        action="store_true",
        help="do not search new courses or lessons - use cache",
    )
    snc.add_argument(
        "--dry-run",
        "-t",
        dest=ArgumentsType.K_DRY_RUN,
        action="store_true",
        help="do not save/download/change anything on disk",
    )

    knowledge_group = snc.add_mutually_exclusive_group()
    knowledge_group.add_argument(
        "--known-courses",
        "-k",
        dest=ArgumentsType.K_KNOWN_COURSES,
        action="store_true",
        help="search new lessons only for known courses",
    )
    knowledge_group.add_argument(
        "--unknown-courses",
        "-u",
        dest=ArgumentsType.K_UNKNOWN_COURSES,
        action="store_true",
        help="search new lessons only for unknown courses",
    )
    knowledge_group.add_argument(
        "--courses",
        metavar="filter",
        dest=ArgumentsType.K_COURSES_FILTERS,
        nargs="+",
        type=lambda query: re.compile(query, re.IGNORECASE),
        help="only consider courses containing one of these filters (regex)",
    )

    download_group = snc.add_mutually_exclusive_group()
    download_group.add_argument(
        "--force-meta",
        "-m",
        dest=ArgumentsType.K_FORCE_META,
        action="store_true",
        help="retrieve lesson metadata even if cached",
    )
    download_group.add_argument(
        "--only-meta",
        "-M",
        dest=ArgumentsType.K_ONLY_META,
        action="store_true",
        help="do not download media streams; update metadata",
    )
    download_group.add_argument(
        "--only-rename",
        "-r",
        dest=ArgumentsType.K_ONLY_RENAME,
        action="store_true",
        help="download nothing; update filenames (implies -n)",
    )

    return parser
