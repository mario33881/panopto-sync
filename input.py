"""
Module containing user-input functions.
"""

import sys
from pathlib import Path
from getpass import getpass
from typing import Tuple, Callable, NamedTuple

from logs import log
from aes_cipher import AESCipher


def ask_non_empty(vardesc: str, prompt: str = None, inputfunc: Callable = None) -> str:
    """
    Asks the user for a non-empty input.
    :param vardesc: field name
    :param prompt: override input prompt
    :param inputfunc: wrapper for input content
    :return: user input
    """

    if prompt is None:
        prompt = f"{vardesc}: "
    if inputfunc is None:
        inputfunc = lambda p: str(input(p))
    errempty = f"{vardesc} must not be empty."
    while True:
        log(prompt, end="")
        sys.stdout.flush()
        ret = inputfunc("")
        if ret != "":
            break
        log(errempty, erase_prev=True)
    return ret


def ask_same(
    match: str,
    vardesc: str,
    prompt: str = None,
    inputfunc: Callable = None,
    errnomatch: str = None,
) -> str:
    """
    Asks the user to enter an input that they presumably entered before.
    :param match: field content to repeat
    :param vardesc: field name
    :param prompt: override input prompt
    :param inputfunc: wrapper for input content
    :param errnomatch: override error message
    :return: user input
    """

    if prompt is None:
        prompt = f"Repeat {vardesc}: "
    if inputfunc is None:
        inputfunc = lambda p: str(input(p))
    if errnomatch is None:
        errnomatch = f"{vardesc}s does not match."
    while True:
        log(prompt, end="")
        sys.stdout.flush()
        ret = inputfunc("")
        if ret == match:
            break
        log(errnomatch, erase_prev=True)
    return ret


def ask_pass(vardesc: str, prompt: str = None) -> str:
    return ask_non_empty(vardesc=vardesc, prompt=prompt, inputfunc=getpass)


def ask_new_pass(vardesc: str, prompt: str = None, promptrepeat: str = None, errnomatch: str = None) -> str:
    ret = ask_non_empty(vardesc=vardesc, prompt=prompt, inputfunc=getpass)
    ask_same(
        match=ret,
        vardesc=vardesc,
        prompt=promptrepeat,
        inputfunc=getpass,
        errnomatch=errnomatch,
    )
    return ret


class Credentials(NamedTuple):
    username: str
    password: str

    @staticmethod
    def generate(p: Path) -> None:
        from aes_cipher import AESCipher

        if p.exists():
            raise FileExistsError("This file already exists.")

        usr = ask_non_empty("Username")
        psw = ask_new_pass("Password")
        log()
        enc = ask_new_pass("File encryption password")

        cipher = AESCipher(enc.encode())
        data = cipher.encrypt_text("\n".join([usr, psw]))
        with p.open(mode="w", encoding="utf-8") as f:
            print(data, file=f)

        log("Success.")

    @staticmethod
    def read(p: Path) -> Tuple["Credentials", str]:
        if not p.is_file():
            raise FileNotFoundError(f"Credentials file does not exists. ({p})")

        cred = None
        while True:
            credkey = ask_pass("Credentials password")
            cipher = AESCipher(credkey.encode())
            with p.open(mode="r", encoding="utf-8") as f:
                crypt = f.read()
            try:
                data = cipher.decrypt_text(crypt)
            except ValueError:
                log("Unable to decrypt credentials.", erase_prev=True)
                continue
            usr, psw = data.splitlines()
            cred = Credentials(username=usr, password=psw)
            break
        log("Accepted.")

        return cred, credkey

    @staticmethod
    def show(p: Path):
        cred, _1 = Credentials.read(p)
        log()
        log("Username:", cred.username)
        log("Password:", cred.password)
