<div align="center">
<h2>PanoptoSync</h2>

Downloader and synchronizer for Panopto and Moodle lessons of University of Verona.
</div>

<br/>

[toc]

<br/>

## Requirements

Here are reported only tested versions; previous versions *may* work, future versions should work.

| Kind  | Description                                     |
|-------|-------------------------------------------------|
| pkg   | Install with your package manager / Windows EXE |
| pip   | Install with `pip`                              |

| Name            |        Kind         | Version                 | Reason                                                 |
|:----------------|:-------------------:|-------------------------|--------------------------------------------------------|
| Python          |  [Native][dl-py3]   | 3.8.2                   |                                                        |
| yt-dlp          | [Native][dl-ytdlp]  | 2022.05.18              | Parallel download of HLS streams and raw videos        |
| FFMpeg          | [Native][dl-ffmpeg] | 5.0                     | Manipulate streams, obtain info                        |
| MKVToolNIX      | [Native][dl-mkvtn]  | v67.0.0 (*Under Stars*) | Merge streams and chapters in a single `mkv` container |
| `pycryptodomex` |         Pip         | 3.15.0                  | Store encrypted credentials and cookies on your disk   |
| `requests`      |         Pip         | 2.27.1                  | Manual HTTP APIs handling                              |
| `progress`      |         Pip         | 1.6                     | Animated CLI progress bars and spinners                |
| `colorama`      |         Pip         | 0.4.4                   | Cross-platform support for ANSI escapes                |
| `greenlet`      |         Pip         | 2.0.1                   | Coroutines for in-process concurrent programming       |
| `ffmpeg-python` |         Pip         | 0.2.0                   | Put FFMpeg arguments together in an easy way           |
| `yt-dlp`        |         Pip         | 2022.6.22.1             | Attach to yt-dlp from Python                           |
| (other)         |         Pip         |                         | (dependencies for mentioned packages)                  |

[dl-py3]:    https://www.python.org/downloads/
[dl-ff]:     https://www.mozilla.org/firefox/download/thanks/
[dl-ytdlp]:  https://github.com/yt-dlp/yt-dlp/releases
[dl-ffmpeg]: https://ffmpeg.org/download.html
[dl-mkvtn]:  https://mkvtoolnix.download/downloads.html

### One-liners

| Kind   | Platform                                                                                          | Command                                                 |
|--------|---------------------------------------------------------------------------------------------------|---------------------------------------------------------|
| Native | <img src="/.repo/arch.png" width="24"/> ArchLinux           | `sudo pacman -S    python yt-dlp ffmpeg mkvtoolnix-cli` |
|        | <img src="/.repo/ubuntu.png" width="24"/> Ubuntu            | `sudo apt install  python3 yt-dlp ffmpeg mkvtoolnix`    |
|        | <img src="/.repo/ms.png" width="24"/> Windows ([chocolatey][dl-choco]) | `choco install     python3 yt-dlp ffmpeg mkvtoolnix`    |
| Python | <img src="/.repo/pypi.png" width="24"/> Pip                   | `pip install -r requirements.txt`                       |

[dl-choco]: https://chocolatey.org/install#install-step2

<br/>

## Usage

### Command line syntax

Main script: `panoptoSync.py`

There are global arguments and subcommands; each subcommand has its own arguments.

You can see all of them with
```shell
$ ./panoptoSync.py -h
$ ./panoptoSync.py --help
$ ./panoptoSync.py <ACTION> -h
```

You may have to explicitly state the python interpreter:
```shell
$ python panoptoSync.py
```

### Watching lessons

Since most lessons contain multiple _video_ streams (webcam + screen) and sometimes professors choose to convey information from another source, every stream will be downloaded; since the viewer would prefer switching video source without having to keep opening another file and seeking to the right spot, all streams will be combined into one single media file format: [Matroska](https://www.matroska.org/index.html) (.mkv)

To open this kind of files, you'll need a modern — _and decent_ — media player. I **strongly** suggest <img src="/.repo/mpv.png" width="24"/> [MPV](https://mpv.io/) but has a sort of learning curve ([these are the keybindings](https://i.imgur.com/LNGJf4u.png) — change video with underscore <kbd>_</kbd>), or you could use <img src="/.repo/vlc.png" width="24"/> [VLC](https://www.videolan.org/vlc/) and change video track through the right-click menu.


### Running

1. Enter your University credentials and a second password to store them (to do just once)

```shell
$ ./panoptoSync.py genid
```

1. Synchronize your lessons (enter the second password first)

```shell
$ ./panoptoSync.py sync
Credentials password: 
Accepted.

Synchronizing lessons...
Web driver has started.
[...]
```


### Raw lessons

If a course contains raw MP4 lessons on Moodle, make sure it has an entry in `sync.json`,
then add the attribute `"search_raws": true` to the entry (and make sure the syntax is correct).

Example:
```json
    ...
    "number": {
        "lessons": {
            ...
        },
        "name": "Course with raw lessons (2020/2021)",
        "search_raws": true,
        "skip": null,
        "year": 2020
    },
    ...
```


### Skipping courses

When a course ends or you no longer have interest in keeping it in sync,
you can make it skip from all operations. To do so, set the attribute `"skip": true` to its entry.

Example:
```json
    ...
    "number": {
        "lessons": {
            ...
        },
        "name": "Old course (2019/2020)",
        "search_raws": null,
        "skip": true,
        "year": 2019
    },
    ...
```


### Filtering courses

In case you're in a rush and temporarly want to sync just one or two courses,
you can use the `--courses` flag: it accepts multiple case-insensitive regexes, results of which will be joined by OR.

Beware: single-quotes `'` are not recognized on Windows.

Example:
```shell
$ ./panoptoSync.py sync --courses  "archi.+elab.+\(2020"  "sicur.+reti.*\(2021"
```


<br/>

## Troubleshooting

* | There's a connection error, but my internet is working. |
  |:--|
  | University servers are offline, in maintenance or rebooting; retry later. |

* | I got `Unable to log in.` but my credentials are correct. |
  |:--|
  | Sometimes it's a server-side problem. |

* | I got a different error. |
  |:--|
  | Please create an Issue or contact us through Telegram. |


<br/>

## Links

* [<img src="/.repo/telegram.png" width="24"/> Telegram group](https://t.me/joinchat/GRpMmE0T-7fcxupH)
* [<img src="/.repo/kofi.png" width="24"/> Donations](https://ko-fi.com/microeinstein) (**really** appreciated!)


<br/>

## Special thanks

Not in order of importance:

* [Lorenzo Bonanni](https://github.com/LorenzoBonanni)
* [Stefano Zenaro](https://gitlab.com/mario33881)
* [Giovanni Tosini — Rumpappa](https://github.com/Rumpappa)

...and everyone else who donated or supported me in some way.
