import shlex
import sys
import os
import inspect
import itertools
import textwrap
from enum import Enum, auto
from types import TracebackType
from typing import List, Type
import colorama
from colorama import ansi  # noqa (import from here to init library)


colorama.init()


# https://stackoverflow.com/a/10455937
if os.name == "nt":
    import ctypes

    class _CursorInfo(ctypes.Structure):
        _fields_ = [("size", ctypes.c_int), ("visible", ctypes.c_byte)]

    CI = _CursorInfo()
    HANDLE = ctypes.windll.kernel32.GetStdHandle(-11)


def hide_cursor(stream=sys.stdout):
    if os.name == "nt":
        ctypes.windll.kernel32.GetConsoleCursorInfo(HANDLE, ctypes.byref(CI))
        CI.visible = False
        ctypes.windll.kernel32.SetConsoleCursorInfo(HANDLE, ctypes.byref(CI))
    elif os.name == "posix":
        stream.write("\033[?25l")
        stream.flush()


def show_cursor(stream=sys.stdout):
    if os.name == "nt":
        ctypes.windll.kernel32.GetConsoleCursorInfo(HANDLE, ctypes.byref(CI))
        CI.visible = True
        ctypes.windll.kernel32.SetConsoleCursorInfo(HANDLE, ctypes.byref(CI))
    elif os.name == "posix":
        stream.write("\033[?25h")
        stream.flush()


def my_exception_hook(exc_type: Type[BaseException], value: BaseException, trace: TracebackType = None, file=sys.stderr):
    reduced = file in (sys.stderr, sys.stdout)
    half, rem = divmod(3 if reduced else 7, 2)

    def err(*aa, **kw): print(*aa, file=file, **kw)
    esc = lambda _code: "\x1b[" + _code

    R = esc('0m')
    msg_banner = lambda _txt: ''.join(( esc('1;41;97m'), esc('K'), ' ', _txt, ' ', R ))
    msg_file = lambda _file, _pos: ''.join(( esc('1;38;5;45m'), _file, R, ':', esc('38;5;39m'), str(_pos), R ))
    msg_func = lambda _cls, _func, _args: ''.join((
        esc('38;5;184m')+_cls+R+'.' if _cls else "",
        esc('1;38;5;198m'), _func,
            ''.join(( '(', R, esc('38;5;178m'), ', '.join(_args), '..', esc('1;38;5;198m'), ')' )) if _args is not None else "",
        R ))
    msg_code = lambda _l, _hl: ''.join(( R,
        esc('48;5;240m') if _hl else esc('48;5;237m'),
        esc('97;1m') if _hl else esc('97m'),
        _l, R ))
    msg_local = lambda _k, _v: ''.join(( esc('37;1m'), _k, R, esc('38;5;244m'), _v ))

    def tryindex(txt, *match):
        for m in match:
            try:
                return txt.index(m)
            except ValueError:
                pass
        return -1

    firstdir = None
    def print_trace(t):
        nonlocal firstdir
        c = t.tb_frame.f_code
        fn = c.co_filename
        sdkl = tryindex(fn, 'lib/python', 'lib\\python')
        if sdkl >= 0:
            return
            # fn = '*'+fn[sdkl:]
        elif firstdir:
            fn = os.path.relpath(fn, firstdir)
        else:
            firstdir = os.path.dirname(fn)
            fn = os.path.basename(fn)
        ln = t.tb_lineno

        scls = None
        sfunc = c.co_name
        largs = None

        # POSITION
        lc = t.tb_frame.f_locals
        if not sfunc.startswith('<') and lc:
            largs = []
            a = c.co_varnames
            obj = lc[a[0]]
            if obj:
                if a[0] == 'self':
                    scls = obj.__class__.__name__
                elif a[0] == 'cls':
                    scls = obj.__name__
            expl = c.co_argcount + c.co_kwonlyargcount
            largs.extend(a[:expl])
            # i += expl
            # if isinstance(lc[a[i]], tuple):
            #     sargs.append('*' + a[i])
            #     i += 1
            # if isinstance(lc[a[i]], dict):
            #     sargs.append('**' + a[i])
        err(f"  {msg_file(fn, ln):<59}    {msg_func(scls, sfunc, largs)}")

        # CODE
        with open(c.co_filename) as fin:
            li = ln-1
            src = '\n'.join(l.rstrip() for i, l in enumerate(
                itertools.islice(fin, li-half, ln+half)))
            src = textwrap.indent(textwrap.dedent(src), '  ', lambda x: True).split('\n')
            mlen = max(max((len(l) for l in src)), 80)
            for i, l in enumerate(src):
                err('  ', msg_code(l.ljust(mlen), i == half), sep='')

        # LOCALS
        filt = {k: v for k, v in lc.items() if not (k.startswith('__') or callable(v) or inspect.ismodule(v))} if lc else None
        if filt:
            kwid = max((len(k) for k in filt.keys()))
            slocs = []
            for k, v in filt.items():
                try:
                    rv = repr(v)
                except BaseException:  # noqa
                    rv = ""
                if not rv or (reduced and len(rv) > 80) or v.__class__.__repr__ == object.__repr__:
                    rv = f"<{v.__class__.__name__}>"
                slocs.append(msg_local(k+'=' if reduced else f"{k:<{kwid}} = ", rv))
            wr = lambda txt: textwrap.wrap(txt, width=140,
                break_long_words=True, break_on_hyphens=True,
                initial_indent='    ', subsequent_indent='    '
            )
            if reduced:
                err(*wr('    '.join(slocs)), sep='\n')
            else:
                err()
                for loc in slocs:
                    err(*wr(loc), sep='\n')

    # OUTPUT
    err()
    err(msg_banner('Traceback'))
    err()
    while trace:
        print_trace(trace)
        err()
        trace = trace.tb_next
    err(msg_banner(f"{exc_type.__name__}: {value}"))

# override the default exception hook
sys.excepthook = my_exception_hook


# try:
#     from rich.traceback import install
#     install(show_locals=True, suppress=[])
# except ImportError:
#     install = None


class Verb(Enum):
    content = auto()
    auth = auto()
    scraping = auto()
    network = auto()
    procs = auto()
    times = auto()
    dates = auto()


class Log:
    def __init__(self, file=sys.stdout, **kwargs):
        self.verbosity = []
        self.level = 0
        self.delta = 2
        self.file = file
        self.print = print
        self.printargs = kwargs

    def __enter__(self):
        self.level += 1
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.level = max(0, self.level - 1)

    def reset_level(self):
        self.level = 0

    def is_verbose(self, flag: Verb):
        return flag in self.verbosity

    @staticmethod
    def get_pad(level, delta) -> str:
        if level < 1:
            return ""
        return " " * (level * delta)

    def log(
        self,
        *args,
        pointer: bool = False,
        verbos: Verb = None,
        erase_prev: bool = False,
        pad: bool = True,
        **kwargs,
    ):
        if verbos and not self.is_verbose(verbos):
            return
        kw = {**self.printargs, **kwargs}
        kw2 = dict(file=self.file, end="")
        kw2.update(kw)
        txtpad = None
        if pad:
            if pointer:
                txtpad = Log.get_pad(self.level - 1, self.delta) + "> "
            else:
                txtpad = Log.get_pad(self.level, self.delta)
        if erase_prev:
            kwe = dict(file=self.file, sep="", end="")
            kwe.update(self.printargs)
            self.print(ansi.Cursor.UP(1), ansi.clear_line(2), **kwe)
            # self.print('§', **kwe)
        if not erase_prev or args:
            if pad:
                self.print(txtpad, **kw2)
            self.print(*args, **kw)
        self.file.flush()

    def errlog(self, *args, **kwargs):
        self.log(*args, file=sys.stderr, **kwargs)


logger: Log = Log()
log = logger.log
errlog = logger.errlog


def dump_args(prefix: str, args: List[str]):
    errlog(verbos=Verb.procs)
    errlog(prefix, "> ", " ".join([shlex.quote(a) for a in args]), sep="", verbos=Verb.procs)
