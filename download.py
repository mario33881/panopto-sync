import io
import os
import re
import math
import itertools
import shlex
import shutil
import glob
import subprocess
from urllib import parse as ulp
from pathlib import Path
from typing import List, Tuple, Set, Optional, IO, Union

import ffmpeg  # noqa
import yt_dlp

from logs import Verb, logger, errlog, dump_args
from core import (
    nice_time,
    digest,
    comparison_chain,
    EasyProcessArgs,
    popen_easy,
    ensure_directory,
    simple_shred,
    is_debugging,
    makesure,
    SimpleEnterExit,
    ErrorLogger,
    Loadable,
)
from web import Cookies, dump_cookies_mozilla_format, SessionEx
from content import Lesson, Stream, Gap, Cut
from progress_bar import CustomMixedBar


# noinspection PyAttributeOutsideInit
# noinspection PyMethodMayBeStatic
class LessonDownloader(Loadable):
    """Designed to be single-use"""

    RGX_BAD_CHAPTER_CAPT = re.compile(r"^[0-9]+$|\.\.\.$")
    RGX_BETTER_CHAPTER_CAPT = re.compile(r"^(?:[^a-z]+)?((?: *(?!the |il )[a-z.]+){1,3})", re.IGNORECASE)
    RGX_QUALITY = re.compile(r"^#EXT-X-STREAM-INF.*[^-]BANDWIDTH=([0-9]+).*$\n^(.+)$", re.MULTILINE)
    RGX_PARTIAL_STREAM = re.compile(r"^_psync_[a-fA-F0-9]+_stream[0-9]+")  # used in sync

    LIST_EXT = ".txt"
    SLIDES_EXT  = ".jpg"
    VIDEOS_EXT  = ".mp4"  # quicktime/mp4
    SEGMENT_EXT = ".ts"  # keep mpegts format to allow perfect segmentation
    OUTPUT_EXT  = ".mkv"  # matroska

    STREAM_FMT = "_psync_{}_stream{}" + VIDEOS_EXT
    GAP_FMT = "_{}_gap{}.base" + SEGMENT_EXT
    CUT_FMT = "_{}_segment{}.{}" + SEGMENT_EXT
    SLIDE_FMT = "_{}_slide{}" + SLIDES_EXT
    CHAPT_FMT = "_{}_chapters" + LIST_EXT
    CONCAT_FMT = "_{}_concat" + LIST_EXT
    SLISHOW_FMT = "_{}_slides" + SEGMENT_EXT

    _DEBUG_ONLY_SEGMENTS = False

    def __init__(
        self,
        lesson: Lesson,
        destdir: Path,
        session: SessionEx,
        dryrun: bool = False,
    ):
        self.lesson: Lesson = lesson
        self.dest_dir = destdir
        self.out_file = lesson.filename  # with extension
        self.session = session
        self.dry_run = dryrun
        self.ffmpeg_loglevel = 'info' if logger.is_verbose(Verb.procs) else 'fatal'
        self.temp_files: List[str]  # delete on failure: yes (unused)
        self.resources: Set[str]  # delete on failure: no
        self.final_media_args: Union[List, EasyProcessArgs]
        self.chapters_file: Optional[str]
        self.slides_data: List[Tuple[str, float]]  # (filename, duration)
        self.old_cwd: str


    def _get_final_segments2(self, stream):
        cuts: List[Cut] = stream.meaningful_segments[:]  # copy
        if not cuts:
            return cuts

        # stream is segmented, so make a gap the entire length of the lesson
        targetlen = stream.lesson.duration - stream.ignored_gap
        gaps: List[Gap] = [Gap(start2=0, end2=targetlen, source_stream=stream)]

        # overlap and fuse cuts
        # must remove older element due to iteration
        for p1, p2 in list(comparison_chain(cuts)):
            if p1.end1 == p2.start1 and p1.offset == p2.offset:
                p2.start1 = p1.start1
                p2.start2 = p1.start2
                cuts.remove(p1)
            ovr2 = p1.end2 - p2.start2
            if ovr2 > 0:
                p1.end2 -= ovr2
                p1.end1 -= ovr2
                if p1.end2 - p1.start2 <= 0:
                    cuts.remove(p1)

        # try enhancing av-sync by shifting start time cumulatively
        # drift = 0
        # for c in cuts:
        #     c.start1 -= drift
        #     c.end1 -= drift
        #     drift += 1000

        # subtract cuts times from gaps
        for v in cuts:
            recheck = True
            while recheck:
                recheck = False
                for z in gaps[:]:  # shallow copy
                    # gap starts in the middle of a cut
                    if v.start2 <= z.start2 <= v.end2:
                        z.start2 = v.end2

                    # gap ends in the middle of a cut
                    if v.start2 <= z.end2 <= v.end2:
                        z.end2 = v.start2

                    # cut starts and finishes in the middle of a gap
                    if z.start2 <= v.start2 and v.end2 <= z.end2:
                        z2 = Gap(start2=v.end2, end2=z.end2, source_stream=stream)
                        if z2.duration2 > 0:
                            gaps.append(z2)
                            recheck = True
                        z.end2 = v.start2

                    if z.duration2 <= 0:
                        gaps.remove(z)

        # accumulate very small gaps
        acc = 0
        for z in gaps[:]:  # shallow copy
            z.end2 += acc
            acc = 0
            if z.duration2 < 500:
                acc += z.duration2
                gaps.remove(z)

        # merge all together
        cuts: List[Union[Cut, Gap]]
        cuts += gaps
        cuts.sort(key=lambda p: p.start2)

        totdur = sum(p.duration2 for p in cuts)
        makesure(lambda: totdur + acc == targetlen, "Total length of final cuts does not match.")

        return cuts


    def _make_matroska(self):
        # go = int(self.globalOffset * 1000)
        # https://mkvtoolnix.download/doc/mkvmerge.html
        mkvargs = [
            ("--title", self.lesson.name),
            ("--chapters", self.chapters_file),
            ("-o", self.out_file),
        ] + self.final_media_args
        LessonDownloader.build_matroska(mkvargs, self.dry_run)


    def _slides_to_video(self):
        # ldur = self.lesson.duration
        squeeze_factor = 10.0

        # create concat file with durations
        concatfile = LessonDownloader.CONCAT_FMT.format(self.lesson.id)
        self.resources.add(concatfile)
        with (open(concatfile, "w") if not self.dry_run else io.StringIO()) as f:
            print("ffconcat version 1.0", file=f)
            for slide, dur in self.slides_data:
                slide = shlex.quote(slide)
                t1 = ("file", slide)
                t2 = ("duration", dur / 1000 / squeeze_factor)
                print(*t1, file=f)
                errlog(*t1, verbos=Verb.procs)
                print(*t2, file=f)
                errlog(*t2, verbos=Verb.procs)

        # consider final file
        finalfile = LessonDownloader.SLISHOW_FMT.format(self.lesson.id)
        self.resources.add(finalfile)
        self.final_media_args.append(("--track-name", "0:SLIDES"))
        self.final_media_args.append(("-y", "0:0," + str(squeeze_factor)))
        self.final_media_args.append((finalfile,))
        if Path(finalfile).is_file():
            return

        # produce video
        with CustomMixedBar(message=CustomMixedBar.make_msg("#s2video")):
            fi = ffmpeg.input(
                concatfile,
                loglevel=self.ffmpeg_loglevel,
                f="concat",
                safe="0",
            )
            fo = fi.output(finalfile, vcodec="h264", tune="stillimage", r=squeeze_factor)
            if not self.dry_run:
                fo.run()

    def _download_slides(self):
        flt = lambda t: (t.is_slide and t.time is not None and t.number is not None and t.number > 0)
        slides_ts = filter(flt, self.lesson.timestamps)
        slides_ts = sorted(slides_ts, key=lambda t: t.time)
        lensl = len(slides_ts)

        for s in slides_ts:
            errlog("slide:", s.slide_url, verbos=Verb.content)

        with CustomMixedBar(message=CustomMixedBar.make_msg("#slides")) as prog:
            prog.steps_mode_init(lensl)
            for i, s in enumerate(slides_ts):
                jpg = LessonDownloader.SLIDE_FMT.format(self.lesson.id, i)
                self.resources.add(jpg)
                p = Path(jpg)

                if not p.exists():
                    with self.session.get(s.slide_url) as resp:
                        if not self.dry_run:
                            with open(jpg, "wb") as f:
                                shutil.copyfileobj(resp.raw, f)

                # ignore appearing time of first slide (it must be shown at start)
                from_time = s.time if i > 0 else 0
                if i + 1 < lensl:
                    next_time = slides_ts[i + 1].time
                else:
                    next_time = self.lesson.duration
                # ffmpeg may complain on duration=0
                duration = max(100, next_time - from_time)  # milliseconds
                self.slides_data.append((jpg, duration))
                prog.steps_hook()


    def _make_gap2(self, strfile: str, segm_out: str, duration: int):
        # take original stream and destroy video (& audio) to preserve original formats
        fin = ffmpeg.input(
            strfile,
            loglevel=self.ffmpeg_loglevel
        )
        fout = ffmpeg.output(
            fin, segm_out,
            vf='eq=brightness=-1:saturation=-1',
            af='volume=-infdB',
            #map=['0:v:0', '0:a?'],
            t=nice_time(duration / 1000),
            vcodec="h264",
            q=0,
            y=None
        )
        dump_args("FFMPEG", fout.get_args())
        if Path(segm_out).exists():
            return
        if not self.dry_run:
            fout.run()

    def _cut_segment(self, strfile: str, segm_out: str, start: int = None, end: int = None):
        fin = ffmpeg.input(
            strfile,
            loglevel=self.ffmpeg_loglevel
        )
        if start and end:
            errlog(segm_out, start / 1000, end / 1000, verbos=Verb.times)
            fout = fin.output(
                segm_out,
                c="copy",
                ss=nice_time(start / 1000),
                to=nice_time(end / 1000),
                q=0,
                y=None,
            )
        else:  # just convert
            fout = fin.output(
                segm_out,
                c="copy",
                q=0,
                y=None,
            )
        dump_args("FFMPEG", fout.get_args())
        if Path(segm_out).exists():
            return
        if not self.dry_run:
            fout.run()

    def _convert_stream(self, strfile: str, segm_out: str):
        return self._cut_segment(strfile, segm_out)

    def _add_concat_stream(self, parts: List[str]):
        # perform concatenation with mkvmerge
        # NOTE: there might be av desync, probably due to imprecise (keyframe) cutting
        #   (vcodec=copy passed to ffmpeg, required to save a lot of time)
        self.final_media_args.append(('[',))
        self.final_media_args.extend(((s,) for s in parts))
        self.final_media_args.append((']',))

    def _prepare_segments(self, parts: List[Union[Cut, Gap]], i: int):
        with CustomMixedBar(message=CustomMixedBar.make_msg("")) as prog:  # no message, see below
            firstgap = next((s for s in parts if isinstance(s, Gap)), None)

            prog.steps_mode_init((1 if firstgap else 0) + len(parts))  # blank + parts

            if firstgap:
                optimal_repeat = 30000
                # generate small gap of 30s (to be repeated)
                prog.message = CustomMixedBar.make_msg("#s:gap0")
                gap_base = LessonDownloader.GAP_FMT.format(self.lesson.id, i)
                self._make_gap2(firstgap.source_stream.write_dest, gap_base, optimal_repeat)
                prog.steps_hook()

            sgm = []
            for si, p in enumerate(parts):
                dur = p.duration2
                makesure(lambda: dur > 0, "Clip duration2 is negative.")
                sout = LessonDownloader.CUT_FMT.format(self.lesson.id, i, si)
                isgap = isinstance(p, Gap)
                prog.message = CustomMixedBar.make_msg("#s:" + ("gap" if isgap else "cut"))
                if isgap:
                    baserep, subdur = divmod(dur, optimal_repeat)
                    errlog(baserep, subdur, verbos=Verb.times)
                    sgm.extend([gap_base] * baserep)
                    subdur = round(subdur / 1000) * 1000
                    if subdur == 0:
                        prog.steps_hook()
                        continue
                    self.resources.add(sout)
                    self._make_gap2(p.source_stream.write_dest, sout, subdur)
                else:
                    self.resources.add(sout)
                    if abs(p.duration2 - p.source_stream.duration2) <= 150:  # use entire stream  [multi scenario]
                        self._convert_stream(p.source_stream.write_dest, sout)
                    else:
                        self._cut_segment(p.source_stream.write_dest, sout, p.start1, p.end1)
                sgm.append(sout)
                prog.steps_hook()

            self._add_concat_stream(sgm)
        return True


    def _prepare_streams(self):
        two_streams = self.lesson.hint.has_separate_webcam_screen()
        streams = self.lesson.meaningful_streams[:]
        if self.lesson.hint == Lesson.Hint.WEBCAM_SCREEN_MERGED:
            del streams[2]  # ignore merged stream
        if self._DEBUG_ONLY_SEGMENTS:
            streams = list((s for s in streams if s.segments or s.all_segm_aligned))

        # download videos phase
        for i, s in enumerate(streams):
            out = LessonDownloader.STREAM_FMT.format(self.lesson.id, i)
            s.write_dest = out
            self.resources.add(out)

            url2 = self.manual_handle_byterange(s.url)
            if not (is_debugging() and os.path.isfile(out)):
                LessonDownloader.ytdlp_progress(url2, out, s.tag, self.session.cookies, self.dry_run)

        # join screen streams
        if not self._DEBUG_ONLY_SEGMENTS and self.lesson.hint == Lesson.Hint.WEBCAM_SCREEN_MULTI:
            chained = list(itertools.chain.from_iterable((s.segments for s in streams[1:])))
            scr = Stream(
                url='',
                tag=f'OBJECTx{len(streams)-1}',
                segments=chained,
                start2=streams[1].start2,
                end2=streams[-1].end2,
            )
            scr.write_dest = streams[1].write_dest  # use first screen recording for gaps
            scr.lesson = self.lesson
            del streams[1:]
            streams.append(scr)

        # handle segments phase
        for i, s in enumerate(streams):
            final_avs = (s.all_segm_aligned or 0) + s.ignored_gap

            # See https://mkvtoolnix.download/doc/mkvmerge.html#d4e1970 (can change!)
            # Track IDs: https://mkvtoolnix.download/doc/mkvmerge.html#mkvmerge.track_ids
            # v65: https://gitlab.com/mbunkus/mkvtoolnix/-/commit/605c0982b781a9045dfbc5a338a3e36fb1934bee
            # booleans: https://gitlab.com/mbunkus/mkvtoolnix/blob/605c0982b781a9045dfbc5a338a3e36fb1934bee/src/common/strings/parsing.cpp#L250
            # keeping older command line switch for retro-compatibility;
            # track IDs CAN be swapped!... ignore default flag for webcam completely
            fma = (
                # all Streams: make every substream non-default
                ("--default-track", "-1:false"),
                # screen Stream: make video as default
                ("--default-track", "0:true" if two_streams and i == 1 else None),

                ("--track-name", "-1:" + s.tag),
                # normal av-sync
                ("-y", f"-1:{final_avs}" if final_avs != 0 else None),
                # hopefully enhance webcam sync (shift audio by +25ms)
                ("-y", "1:25" if i == 0 and two_streams else None),
            )

            self.final_media_args.extend(fma)

            parts = self._get_final_segments2(s)
            for p in parts:
                errlog(repr(p), verbos=Verb.times)
            if not parts:
                self.final_media_args.append((s.write_dest,))
                continue
            self._prepare_segments(parts, i)


    def _make_subtitles(self):
        pass

    def _make_slide_chapters(self):
        fslide = lambda ts: ts.is_slide
        slide_chapters = filter(fslide, self.lesson.timestamps)
        slide_chapters = sorted(slide_chapters, key=lambda ts: ts.time)
        if not slide_chapters:
            return
        self.chapters_file = LessonDownloader.CHAPT_FMT.format(self.lesson.id)
        self.resources.add(self.chapters_file)

        chap_num_fmt = f"0{int(math.log10(len(slide_chapters)))}d"
        with (open(self.chapters_file, mode="w", encoding="utf-8") if not self.dry_run else io.StringIO()) as f:
            i = 1
            for ch in slide_chapters:
                td = nice_time(ch.time)  # ( + self.globalOffset)
                capt = ch.caption
                if capt is not None:
                    capt = capt.splitlines()[0]
                    if LessonDownloader.RGX_BAD_CHAPTER_CAPT.search(capt):
                        capt2 = LessonDownloader.RGX_BETTER_CHAPTER_CAPT.search(ch.content or "")
                        if capt2:
                            capt = capt2[1].title()
                else:
                    capt = str(i)
                print(f"CHAPTER{i:{chap_num_fmt}}={td}", file=f)
                print(f"CHAPTER{i:{chap_num_fmt}}NAME={capt}", file=f)
                i += 1


    def cleanup(self):
        """delete temporary files"""
        if not self.dry_run:
            for f in self.temp_files:
                os.remove(f)

    def load(self):
        self.temp_files = set()
        self.resources = set()
        self.final_media_args = []
        self.chapters_file = None
        self.slides_data = []
        self.old_cwd = os.getcwd()
        if not self.dry_run:
            ensure_directory(self.dest_dir)
            os.chdir(str(self.dest_dir))  # change dir

    def run(self, keep_resources: bool = False):
        lens = len(self.lesson.meaningful_streams)
        makesure(lambda: lens > 0, "Lesson does not contain any stream")

        self._prepare_streams()
        self._make_subtitles()

        if self.lesson.hint == Lesson.Hint.AUDIO_SLIDES:
            self._download_slides()
            self._slides_to_video()
            self._make_slide_chapters()

        # WARNING
        # delete existing output if present
        out_path = Path(self.out_file)
        if out_path.exists():
            if out_path.is_dir():
                self.cleanup()
                raise FileExistsError("Output path is blocked by a directory!")
            if not self.dry_run:
                out_path.unlink()

        self._make_matroska()

        if not (self.dry_run or keep_resources):
            # delete resources
            for f in self.resources:
                os.remove(f)

    def unload(self):
        self.cleanup()
        if not self.dry_run:
            os.chdir(self.old_cwd)  # restore dir
        del self.temp_files
        del self.final_media_args
        del self.chapters_file
        del self.slides_data


    def manual_handle_byterange(self, url: str) -> str:
        # get master playlist
        urlparts = ulp.urlparse(url)
        if not urlparts.path.endswith("master.m3u8"):
            return url
        data: str
        with self.session.get(url) as resp:
            data = resp.text

        # get max quality
        quals = LessonDownloader.RGX_QUALITY.findall(data)
        if not quals:
            return url
        max_qual: str = sorted(quals, key=lambda m: m[1], reverse=True)[0][1]

        # get fragments playlist
        if not max_qual.endswith("index.m3u8"):
            return url

        # check for byterange
        url2 = ulp.urlunparse(urlparts._replace(path=ulp.urljoin(urlparts.path, max_qual)))
        with self.session.get(url2) as resp:
            for line in resp.iter_lines(512):
                line = bytes.decode(line)
                if not (line.startswith("#EXT-X-MAP:") and "BYTERANGE" in line and "URI" in line):
                    continue
                for arg in line.split(","):
                    k: str
                    v: str
                    k, v = arg.split("=")
                    if not k.endswith("URI"):
                        continue
                    return ulp.urljoin(url2, shlex.split(v)[0])  # found

        # failure, fallback to original url and behaviour
        return url

    @staticmethod
    def ytdlp_progress(url: str, out: str, label: str, cookies: Cookies = None, dry: bool = False):
        cookiefile = digest(url.encode(), digest_size=12)

        def dump_cookies():
            if not dry and cookies is not None:
                dump_cookies_mozilla_format(cookies, cookiefile)

        def shred_cookies():
            if not dry and os.path.exists(cookiefile):
                simple_shred(cookiefile)

        with SimpleEnterExit(dump_cookies, shred_cookies):
            with CustomMixedBar(message=CustomMixedBar.make_msg(label or "#yt-dlp")) as prog:
                ydl_opts = dict(
                    quiet=True,
                    logger=ErrorLogger(),
                    outtmpl=out,
                    overwrites=True,
                    external_downloader=dict(m3u8='native'),
                    concurrent_fragment_downloads=(os.cpu_count() or 4) * 2,
                    progress_hooks=[prog.progress_hook],
                )
                if cookies is not None:
                    ydl_opts["cookiefile"] = cookiefile
                if not dry:
                    for i in range(2):
                        try:
                            with yt_dlp.YoutubeDL(ydl_opts) as ydl:
                                ydl.download([url])
                        except yt_dlp.DownloadError as ex:
                            # not recoverable, delete partial fragments
                            for filename in glob.glob(f"{out}*"):
                                os.remove(filename)
                            if i == 1:
                                raise ex
                            # continue
                        else:
                            break

                prog.progress_hook_finished()

    @staticmethod
    def build_matroska(mkvargs: list, dry: bool = False):
        mkvargs.insert(0, ("mkvmerge",))
        mergeproc = popen_easy(
            mkvargs,
            stdin=subprocess.DEVNULL,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            bufsize=0,
            universal_newlines=True,
            shell=False,
        )
        stdout: IO = mergeproc.stdout
        with CustomMixedBar(message=CustomMixedBar.make_msg("#matroska")) as prog:
            buffout = ""
            allout = ""
            if not dry:
                while mergeproc.poll() is None:
                    txt = stdout.read(3)
                    buffout += txt
                    allout += txt
                    end = prog.mkvmerge_progress(buffout)
                    if end is not None:
                        buffout = buffout[end:]
                allout += stdout.read()
                errlog("\n", allout, sep="", verbos=Verb.procs)
                if mergeproc.returncode != 0:
                    raise subprocess.SubprocessError(f"Process returned {mergeproc.returncode}.\n\n{allout}")
