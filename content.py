import os
import datetime as dt
from abc import ABCMeta, ABC, abstractmethod
from enum import Enum, auto
from dataclasses import dataclass, field
from functools import cached_property
from pathlib import Path
from typing import (
    List,
    Dict,
    KeysView,
    Set,
    Mapping,
    Optional,
    Union,
    Any,
    Type,
    TypeVar,
    Generic,
    get_origin,
)


from logs import Verb, logger, errlog
from core import comp_ms_time, makenames, makenone, StrEnum #, digest
from title_format import extract_course_year, ORIGINAL_FILENAME_FMT, pathify_course_name


JsonDict = Dict[str, Any]


class IDeepUpdate(ABC):
    @abstractmethod
    def deep_update(self, other):
        ...


class RecordMeta(ABCMeta):  # use `ABCMeta` instead of `type` to avoid conflicts
    def __delattr__(self: "RecordMeta", item: str):
        """Hook dataclass field attribute deletion and set new class defaults."""
        super().__delattr__(item)
        makenone(self, name=item)


class Record(IDeepUpdate, metaclass=RecordMeta):
    @staticmethod
    def __getannotation__(cls: Type, name: str):
        for hier in cls.mro():
            try:
                notes = hier.__annotations__
            except AttributeError:
                continue
            if name in notes:
                return notes[name]
        return None

    @classmethod
    def __init_subclass__(cls, /, **kwargs):
        super().__init_subclass__(**kwargs)
        for subcl in cls.mro():
            makenames(cls, subcl)
            makenone(cls, subcl)

    def __delattr__(self, item: str):
        # not annotated:
        #   (not False or ...)
        #   -> error if already deleted
        #
        # annotated and missing:
        #   (not True or False)
        #   -> do nothing (has None default)
        #
        # annotated and present:
        #   (not True or True)
        #   -> delete from instance
        if not hasattr(self, item) or item in self.__dict__:
            super().__delattr__(item)

    @classmethod
    def new(cls, workdict: Union[JsonDict, "Record"] = None, raw=False):
        ret = cls.__new__(cls)
        if workdict:
            if isinstance(workdict, Record):
                workdict = workdict.plain()
            ret.__dict__ = workdict
        if not raw:
            ret._before_new(ret.__dict__)
        return ret

    def deep_update(self, other: "Record"):
        for k, v2 in other.plain().items():
            v1 = self.plain().get(k)
            if v1:
                if v1 is v2:  # identical objects
                    continue  # avoid clash with FieldsFuse behavior
                if isinstance(v1, IDeepUpdate) and isinstance(v2, IDeepUpdate):
                    v1.deep_update(v2)
                    continue
            self.plain()[k] = v2

    def plain(self) -> JsonDict:
        return self.__dict__

    def dump(self) -> JsonDict:
        wrap = self.plain().copy()
        ret = self.__class__.new(wrap, True)
        ret._before_dump(wrap)
        return wrap

    def _before_new(self, wrap: JsonDict):
        for k, v in wrap.items():
            if not isinstance(v, dict):
                continue
            note = Record.__getannotation__(self.__class__, k)
            note = get_origin(note) or note
            if note and issubclass(note, Record):
                setattr(self, k, note.new(v))

    def _before_dump(self, wrap: JsonDict):
        for k, v in wrap.items():
            if isinstance(v, Record):
                setattr(self, k, v.dump())


class FieldsFuse:
    __setonce__: Set[str]

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        base_sets = list(filter(None, map(lambda bc: getattr(bc, "__setonce__", None), cls.mro())))
        cls.__setonce__.update(*base_sets)

    def __setattr__(self, key, value):
        flds = self.__class__.__setonce__
        if key in flds and key in self.__dict__:
            raise AttributeError(f"Tried to set '{key}' twice.")
        super().__setattr__(key, value)

    def __delattr__(self, key):
        flds = self.__class__.__setonce__
        if key in flds and key in self.__dict__:
            raise AttributeError(f"Tried to delete '{key}'.")
        super().__delattr__(key)


Tk = TypeVar("Tk")
Tv = TypeVar("Tv")


class HooksSetting(ABC, Dict[Tk, Tv], Generic[Tk, Tv]):
    @abstractmethod
    def __sethook__(self, k: Tk, v: Tv):
        ...

    def __setitem__(self, k: Tk, v: Tv):
        super().__setitem__(k, v)
        self.__sethook__(k, v)

    def update(self, __m: Mapping[Tk, Tv], **kwargs):
        for k, v in __m.items():
            self.__sethook__(k, v)
        super().update(__m, **kwargs)


K_ID = "id"


@dataclass
class IdentifiedRecord(Record, FieldsFuse):
    __setonce__ = {K_ID}
    id: str

    @classmethod
    def from_raw_dict(cls, data: JsonDict) -> "RecordSet":
        ret = RecordSet()
        for k, v in data.items():
            pv = cls.new(v)
            ret[k] = pv  # set id by hook
        return ret

    @classmethod
    def to_raw_dict(cls, data: "RecordSet") -> JsonDict:
        ret = dict()
        for k, v in data.items():
            v: Record
            pd = v.dump()
            del pd[K_ID]
            ret[k] = pd
        return ret


Tid = TypeVar("Tid", bound=IdentifiedRecord)


class RecordSet(HooksSetting[str, Tid], IDeepUpdate, Generic[Tid]):
    def __getitem__(self, item: str) -> Tid:
        return super().__getitem__(item)

    def get(self, item: str) -> Optional[Tid]:
        return super().get(item)

    def keys(self) -> KeysView[str]:
        return super().keys()

    def __sethook__(self, k: str, v: Tid):
        v.__dict__[K_ID] = k

    def deep_set(self, other):
        if not isinstance(other, IdentifiedRecord):
            raise AttributeError("Trying to set an invalid kind of object.")
        k = other.__dict__[K_ID]
        v1 = self.get(k)
        if v1:
            if v1 is other:  # identical objects
                return
            if isinstance(v1, IDeepUpdate) and isinstance(other, IDeepUpdate):
                v1.deep_update(other)
                return
        self[k] = other

    def deep_update(self, other: "RecordSet"):
        for k, v2 in other.items():
            self.deep_set(v2)


@dataclass
class RawEntry:
    url: str
    title: str
    label: str
    header: str


@dataclass
class Timestamp:
    kind: str  # known: ObjectVideo, Primary, PowerPoint
    is_slide: bool
    number: int  # valid if >0
    caption: str  # small excerpt of content (usually only on slides)
    content: Optional[str]  # various text taken from slide
    time: int  # ms
    thumb_url: str  # doesn't need cookies
    slide_url: str  # doesn't need cookies Nor any identification header


@dataclass
class Gap:
    start2: int
    end2: int
    source_stream: "Stream" = None

    @property
    def duration2(self): return self.end2 - self.start2

    def __repr__(self):
        return str.format(
            "  ({} ~ {} = {})",
            comp_ms_time(self.start2),
            comp_ms_time(self.end2),
            comp_ms_time(self.duration2),
        )


@dataclass
class Cut:
    start1: int
    end1: int
    start2: int
    end2_offset: int = 0
    source_stream: "Stream" = None

    @property
    def duration1(self): return self.end1 - self.start1

    @property
    def offset(self): return self.start2 - self.start1  # unbounded

    @property
    def end2(self): return self.end1 + self.offset + self.end2_offset

    @end2.setter
    def end2(self, value: int): self.end2_offset = value - self.end2

    @property
    def duration2(self): return self.end2 - self.start2

    def __repr__(self):
        return str.format(
            "§ ({} ~ {} = {}) ← ({} ~ {} = {})",
            comp_ms_time(self.start2),
            comp_ms_time(self.end2),
            comp_ms_time(self.duration2),
            comp_ms_time(self.start1),
            comp_ms_time(self.end1),
            comp_ms_time(self.duration1),
        )


@dataclass
class Stream:
    class Tag(StrEnum):
        AUDIO = auto()
        DV = auto()
        SCREEN = auto()
        OBJECT = auto()
        RAW = auto()  # custom

    url: str  # doesn't need cookies
    tag: str
    start2: Optional[int] = None
    end2: Optional[int] = None
    ignored_gap: int = field(init=False)
    segments: List[Cut] = field(default_factory=list)
    lesson: "Lesson" = field(init=False)
    write_dest: str = field(init=False)

    @property
    def duration2(self):
        return self.end2 - self.start2 if None not in (self.start2, self.end2) else None

    @cached_property
    def all_segm_aligned(self) -> Optional[int]:
        sg = self.segments
        if sg:
            lsg = len(sg)
            ofs = tuple((s.offset for s in sg))
            s0 = ofs[0]
            # all segments have the same offset
            if ofs.count(s0) == lsg:
                return s0
        return None

    @cached_property
    def meaningful_segments(self) -> List[Cut]:
        if self.all_segm_aligned is not None:
            return []
        return self.segments

    def __post_init__(self):
        sg = self.segments
        opt = 0 if not sg else sg[0].start2
        if opt > 500:
            opt = 0
        self.ignored_gap = opt
        for g in sg:
            g.source_stream = g.source_stream or self  # do not change if already set
            g.start2 -= opt


@dataclass
class Comment:
    user: str
    text: str
    time: int  # ms (position inside lesson)
    creation: str  # date


@dataclass
class Lesson(IdentifiedRecord):
    """
    Lesson instances are created by reading from cache (partial instance)
    or by fetching Panopto (full instance) --- the full instance will
    replace the partial one in :class:`Synchronizer._retrieve_panopto_lesson_entry()`
    """

    class Hint(Enum):
        UNKNOWN = auto()
        SIMPLE = auto()
        EMBED = auto()
        AUDIO_SLIDES = auto()
        MERGED_SINGLE = auto()
        AUDIO_SCREEN = auto()
        WEBCAM_SCREEN = auto()
        WEBCAM_SCREEN_MERGED = auto()
        WEBCAM_SCREEN_MULTI = auto()

        def has_separate_webcam_screen(self) -> bool:
            H = Lesson.Hint
            return self in (H.WEBCAM_SCREEN, H.WEBCAM_SCREEN_MERGED, H.WEBCAM_SCREEN_MULTI)

        # def stream0_has_video(self) -> bool:
        #     H = Lesson.Hint
        #     return self in (
        #         H.SIMPLE,
        #         H.MERGED_SINGLE,
        #         H.WEBCAM_SCREEN,
        #         H.WEBCAM_SCREEN_MERGED,
        #     )

        @classmethod
        def find_hint(cls, lesson: "Lesson") -> "Lesson.Hint":
            H = Lesson.Hint
            if lesson.is_raw:
                return H.SIMPLE

            lens = len(lesson.streams)

            if any(map(lambda s: not s.url, lesson.streams)):
                return H.EMBED

            T = Stream.Tag
            if lens == 1:
                s0 = lesson.streams[0]
                if s0.tag == T.AUDIO and any((t.is_slide for t in lesson.timestamps)):
                    return H.AUDIO_SLIDES
                if s0.tag == T.DV:
                    return H.MERGED_SINGLE

            elif lens == 2:
                s0, s1 = lesson.streams[0 : 1 + 1]
                tagtuple = (s0.tag, s1.tag)
                if tagtuple == (T.AUDIO, T.SCREEN):
                    return H.AUDIO_SCREEN
                if tagtuple == (T.DV, T.OBJECT) or tagtuple == (T.DV, T.SCREEN):
                    return H.WEBCAM_SCREEN

            elif lens > 3:
                sother = lesson.streams[1:]
                if lesson.streams[0].tag == T.DV and all((s.tag == T.OBJECT for s in sother)):
                    if lens == 3 and all((s.ignored_gap == sother[0].ignored_gap for s in sother)):
                        return H.WEBCAM_SCREEN_MERGED
                    return H.WEBCAM_SCREEN_MULTI

            return H.UNKNOWN

    name: str
    is_raw: bool
    date: dt.datetime
    duration: Optional[int] = None  # ms, None for raw
    authors: List[str] = field(default_factory=list)
    comments: List[Comment] = field(default_factory=list)
    streams: List[Stream] = field(default_factory=list)
    podcast_streams: List[Stream] = field(default_factory=list)  # embedded all-in-one video (lower quality)
    timestamps: List[Timestamp] = field(default_factory=list)
    # don't expect timestamps to be sorted by sequence number,
    # you may find 1->2->0->3

    # RUNTIME
    hint: Hint = Hint.SIMPLE
    name_fixed: str = field(init=False)
    #name_hash: str = field(init=False)
    date_name: dt.datetime = field(init=False)
    date_better: dt.datetime = field(init=False)
    filename_new: str = field(init=False)

    # CACHE
    filename: str = field(init=False)
    from_cache: bool = False

    # ATTR NAMES (will be overwritten) (no annotations)
    # (see Record.__init_subclass__: makenames())
    K_DATE = ""
    K_DATE_NAME = ""
    K_DATE_BETTER = ""
    K_NAME = ""
    K_NAME_FIXED = ""
    K_FILENAME = ""
    K_FILENAME_NEW = ""

    @cached_property
    def meaningful_streams(self):
        # hint assigned in __post_init__
        return self.podcast_streams if self.hint == Lesson.Hint.EMBED else self.streams

    def _del_before_new(self):
        del self.streams
        del self.podcast_streams
        del self.meaningful_streams  # noqa  cached_property
        del self.timestamps
        # del self.socialurl

    def _del_before_dump(self):
        self._del_before_new()
        del self.hint
        del self.name_fixed
        #del self.name_hash
        del self.date_name
        del self.date_better
        del self.filename_new
        del self.comments
        del self.from_cache

    def _before_new(self, wrap):
        self._del_before_new()
        super()._before_new(wrap)
        self.from_cache = True
        for k, v in wrap.items():
            if k in (self.K_DATE,):
                v: float
                if v is not None:
                    wrap[k] = dt.datetime.fromtimestamp(v)  # read in localtime
        self.__post_init__()

    def _before_dump(self, wrap):
        self._del_before_dump()
        super()._before_dump(wrap)
        for k, v in wrap.items():
            if k in (self.K_DATE,):
                v: dt.datetime
                if v is not None:
                    wrap[k] = v.timestamp()  # save in localtime

    def __post_init__(self):
        if self.streams:
            for s in self.streams:
                s.lesson = self
        self.assign_hint()
        self.assign_better_date_and_name()
        #self.name_hash = digest((self.name + str(self.date)).encode(), digest_size=12)

    def assign_better_date_and_name(self):
        # ret = extract_better_date_and_name(self.date, self.name)
        # self.name_fixed, self.date_name, self.date_better = ret
        self.name_fixed = self.name
        self.date_name = self.date
        self.date_better = self.date

    def assign_hint(self):
        if self.streams:
            self.hint = Lesson.Hint.find_hint(self)


@dataclass
class Course(IdentifiedRecord):
    name: str
    skip: bool = False
    search_raws: bool = False
    lessons: RecordSet[Lesson] = field(default_factory=RecordSet)

    name_clean: str = field(init=False)
    year: int = field(init=False)
    dir: Path = field(init=False)

    def _before_new(self, wrap):
        del self.name_clean
        del self.dir
        super()._before_new(wrap)

        empty = list(filter(lambda lid: not self.lessons[lid], self.lessons.keys()))
        errlog("Loading:", self.name, verbos=Verb.content)
        with logger:
            for k in empty:
                errlog("deleting empty:", k, verbos=Verb.content)
                del self.lessons[k]
            errlog(verbos=Verb.content)

        ll = Lesson.from_raw_dict(self.lessons)
        self.lessons: RecordSet[Lesson] = ll

    def _before_dump(self, wrap):
        del self.name_clean
        del self.dir
        super()._before_dump(wrap)
        ll = Lesson.to_raw_dict(self.lessons)
        self.lessons: Dict = ll

    def __post_init__(self):
        self.assign_year()

    def assign_year(self):
        self.year, self.name_clean = extract_course_year(self.name)

    def assign_dir(self, basedir: Path):
        self.dir = Path(os.path.abspath(basedir), pathify_course_name(self.name))

    def resolve(self, lesson: Lesson):
        return Path(self.dir, lesson.filename)


class Options(Record):
    VERSION = 1591551221
    downloaderVersion: int  # unix timestamp
    lessonsFilenameFormat: str = ORIGINAL_FILENAME_FMT
