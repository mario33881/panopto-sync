import os
import re
import http.cookiejar
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from urllib import parse as ulp
from http.client import HTTPConnection
from html.parser import HTMLParser
from functools import partial
from typing import List, Tuple, Dict, Mapping, Union, Generic, TypeVar, Optional, Iterator, Callable, Iterable

import requests as reqs
import requests.cookies
# import requests.adapters
from greenlet import greenlet

from core import INITIAL_CWD, get_file_time, makesure, now_path
from logs import Verb, logger, errlog

StrMap = Mapping[str, str]

Cookies = reqs.cookies.RequestsCookieJar
# format: [{'name':n, 'value':v, 'domain':d, 'path':p, 'expiry':e, 'secure':s}, ]

Body = Union[bytes, str]


FAKE_USERAGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0"
POST_REQ_GZIP_HEADERS = {"Accept-Encoding": "gzip"}
HTTP_VER = getattr(HTTPConnection, '_http_vsn_str')
DIR_DUMP_REQUESTS = os.path.join(INITIAL_CWD, "http")


def dump_cookies_mozilla_format(cookies: Cookies, outfile: str):
    jar = http.cookiejar.MozillaCookieJar(outfile)
    for c in cookies:
        jar.set_cookie(c)
    jar.save(outfile, True, True)


def print_request(req: reqs.PreparedRequest, ff: str):
    with open(ff, "w") as fout:
        print(req.method, req.url, HTTP_VER, file=fout)
        for k, v in req.headers.items():
            print(k, v, sep=': ', file=fout)
        print(file=fout)
    if req.body:
        with open(ff, "a" + ("b" if isinstance(req.body, bytes) else "")) as fout:
            fout.write(req.body)


def print_response(resp: reqs.Response, ff: str):
    with open(ff, "w") as fout:
        print(HTTP_VER, resp.status_code, resp.reason, file=fout)
        for k, v in resp.headers.items():
            print(k, v, sep=': ', file=fout)
        print(file=fout)
    if resp.content:  # this will nullify Stream parameter of requests
        with open(ff, "ab") as fout:
            fout.write(resp.content)


def dump_http(*, kind: str, obj):
    os.makedirs(DIR_DUMP_REQUESTS, exist_ok=True)
    fname = f"{now_path()}.{kind}"
    ff = os.path.join(DIR_DUMP_REQUESTS, fname)
    # delete old files (keep 19 + new dump)
    olds = list()
    for curdir, dirs, files in os.walk(DIR_DUMP_REQUESTS):
        for f in files:
            if not f.endswith(kind):
                continue
            curfile = os.path.join(curdir, f)
            filetime = get_file_time(curfile)
            olds.append((curfile, filetime))
    olds.sort(key=lambda t: t[1], reverse=True)
    for fo, fotime in olds[20 - 1 :]:
        os.remove(fo)
    if isinstance(obj, reqs.PreparedRequest):
        print_request(obj, ff)
    if isinstance(obj, reqs.Response):
        print_response(obj, ff)


dump_http_req = partial(dump_http, kind="send.http")
dump_http_resp = partial(dump_http, kind="recv.http")


class SessionEx(reqs.Session):
    LOW_ARGS = dict(
        allow_redirects=False,  # manual handling of 30x codes
        stream=True,  # download content only on field access [must close connection]
        timeout=(4, 6),  # connection and data timeout in seconds
    )

    def __init__(self):
        super().__init__()
        self.hooks['response'].append(SessionEx._debug)

    # noinspection PyUnusedLocal
    @staticmethod
    def _debug(resp: reqs.Response, *a, **kw):
        dump_http_req(obj=resp.request)
        #dump_http_resp(head=resp.headers, body=resp.raw)  # disabled for now
        url = ulp.urlparse(resp.url)
        with logger:
            errlog(url.netloc, url.path, url.query[:40], pointer=True, sep='\t', verbos=Verb.network)

    def request(
        self,
        method: str,
        url: Union[str, bytes],
        *,
        params: Union[Dict, bytes] = None,  # GET url query params
        data: Union[Dict, List[Tuple], bytes] = None,  # POST body
        headers: Dict = None,
        json=None,  # POST body as json
        no_errs=True,
        strict200=True,
        **kw,
    ) -> reqs.Response:
        for k, v in SessionEx.LOW_ARGS.items():
            kw.setdefault(k, v)
        resp = super().request(method=method, url=url, params=params, data=data, headers=headers, json=json, **kw)
        if no_errs:
            resp.raise_for_status()
        if strict200:
            errgen = lambda msg: reqs.HTTPError(msg, response=resp)
            makesure(lambda: resp.status_code == 200, "Response is not successful", errgen)
        return resp

    def head(self, *a, **kw) -> reqs.Response:
        return self.request('head', *a, **kw)

    def get(self, *a, **kw) -> reqs.Response:
        return self.request('get', *a, **kw)

    def post(self, *a, **kw) -> reqs.Response:
        return self.request('post', *a, **kw)


HTMLAttrs = List[Tuple[str, Optional[str]]]
TagFilt = Union[str, Callable[[str], bool], None]

@dataclass(repr=False, eq=False)
class HTMLNode:
    tag: str
    is_void: bool
    attrs: Dict[str, str] = field(default_factory=dict)
    parent: "HTMLNode" = None
    childs: List["HTMLNode"] = field(default_factory=list)
    data: List[Union[str, "HTMLNode"]] = field(default_factory=list)

    def __getitem__(self, item: str) -> str:
        return self.attrs[item]

    def __iter__(self):
        return iter(self.childs)

    def __repr__(self):
        aa = ' '.join(f'{a}="{b}"' for a, b in self.attrs.items())
        return f"<{self.tag} {aa}>({len(self.childs)} childs)</{self.tag}>"

    @property
    def text(self):
        return ''.join(((d if isinstance(d, str) else d.text) for d in self.data))

    def search1(self, _tag: TagFilt, /, **attrs: TagFilt):
        return HTMLIter.search_iter(self, _tag, **attrs)

    def search(self, _tag: TagFilt, /, **attrs: TagFilt):
        yield from self.search1(_tag, **attrs)
        for c in self.childs:
            yield from c.search(_tag, **attrs)

class HTMLIter(HTMLParser):
    """Bottom-up parser"""
    VOIDS = 'area base br col embed hr img input keygen link meta param source track wbr'.split()

    _stack: List[HTMLNode]
    _prev: Optional[HTMLNode]
    _last: Optional[HTMLNode]

    def __init__(self, data: str):
        super().__init__(convert_charrefs=True)
        self._run = lambda: self.feed(data)
        self._coro = None

    def __iter__(self):
        if not self._coro:  # resume iteration
            self._stack = []
            self._prev = None
            self._last = None
            self._coro = greenlet(self._run)
        return self

    def __next__(self) -> HTMLNode:
        val = self._coro.switch()
        if val is None:
            self._coro = None
            raise StopIteration()
        return val

    @staticmethod
    def search_iter(_it: Iterable[HTMLNode], _tag: TagFilt, /, **attrs: TagFilt) -> Iterator[HTMLNode]:
        def build(v):
            if v is None: return lambda a: True
            if isinstance(v, str): return lambda a: a == v  # note: class attribute is space-separated
            if isinstance(v, Callable): return v
            raise ValueError()

        tfilt = build(_tag)
        attrs: Dict[str, Callable[[str], bool]] = {
            (k[1:] if k.startswith('_') else k): build(v)
            for k, v in attrs.items()
        }
        for obj in _it:
            if tfilt(obj.tag) and all((
                k in obj.attrs and v(obj.attrs[k]) for k, v in attrs.items()
            )):
                yield obj

    def search(self, _tag: TagFilt, /, **attrs: TagFilt) -> Iterator[HTMLNode]:
        return HTMLIter.search_iter(self, _tag, **attrs)

    def _yield(self, node: HTMLNode):
        self._coro.parent.switch(node)
        self._prev = node

    def handle_starttag(self, tag: str, attrs: HTMLAttrs) -> None:
        p = self._stack[0] if self._stack else None
        t = HTMLNode(tag=tag, is_void=(tag in HTMLIter.VOIDS), attrs=dict(attrs), parent=p)
        self._last = t
        if p:
            p.childs.append(t)
        if t.is_void:
            return self._yield(t)
        self._stack.insert(0, t)

    def handle_startendtag(self, tag: str, attrs: HTMLAttrs) -> None:
        self.handle_starttag(tag, attrs)
        if not self._last.is_void:
            return self._yield(self._last)

    def handle_endtag(self, tag: str) -> None:
        if tag in HTMLIter.VOIDS:
            if self._last and self._last.is_void and self._last.tag != tag:
                raise ValueError("Invalid HTML.")
            return  # ignore, already yielded
        while self._stack:  # also handles some invalid html
            t = self._stack.pop(0)
            self._yield(t)
            if t.tag == tag:
                break

    def handle_data(self, data: str) -> None:
        if self._stack:
            t = self._stack[0]
            t.data.append(data)
            self._recur_data(t)

    @staticmethod
    def _recur_data(node: HTMLNode):
        """Propagate nodes with text data to parents"""
        if not node.parent:
            return
        HTMLIter._recur_data(node.parent)
        if node not in node.parent.data:
            node.parent.data.append(node)


@dataclass
class AuthProcedure(ABC):
    session: SessionEx
    domain: str
    LABEL = None  # type: str  # (avoid dataclass handling)
    TEST_PATH = None  # type: str
    RGX_DOMAIN = re.compile(r"^https?://(?:[a-z0-9_]@)?([^@/?&#%:]+)(?::[0-9]+)?")

    def urljoin(self, path: str) -> str:
        return f"https://{self.domain}{path}"

    def is_authorized(self) -> Tuple[bool, Optional[str]]:
        assert self.TEST_PATH is not None
        with self.session.get(self.urljoin(self.TEST_PATH), strict200=False) as response:
            if not response.is_redirect:
                return True, None
            loc = response.next.url
            locdom = AuthProcedure.RGX_DOMAIN.search(loc)
            return locdom and locdom[1] == self.domain, loc

    @abstractmethod
    def authorize(self, redir: str = None) -> bool:
        ...


@dataclass
class AbstractSSOAuth(AuthProcedure, ABC):
    @abstractmethod
    def login_portal(self, redirect: str) -> str:
        """
        :param redirect: Target URL returned by other platforms
        :return: New target URL returned by SSO portal
        """
        ...

    def authorize(self, redir: str = None) -> bool:
        raise RuntimeError("Platform does not support direct authorization.")


class StaticUrls:
    BASEURL = "{}"

    def __init__(self, domain: str):
        https = "https://" + domain
        for cls in self.__class__.mro():
            for k, v in cls.__dict__.items():
                if not k.startswith("_") and isinstance(v, str):
                    setattr(self, k, v.format(https))


Tsu = TypeVar("Tsu", bound=StaticUrls)


@dataclass
class Platform(ABC, Generic[Tsu]):
    class Urls(StaticUrls):
        ...

    domain: str
    session: SessionEx
    sso_auth: AbstractSSOAuth
    urls: Tsu = field(init=False)
    auth: AuthProcedure = field(init=False)

    def __post_init__(self):
        self.urls = self.Urls(self.domain)
