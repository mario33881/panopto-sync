import json
import re
import datetime as dt
import urllib.parse as ulp
from abc import ABC, abstractmethod
from dataclasses import dataclass
from enum import Enum
from typing import Dict, List, Tuple, Optional

from core import win_to_unix_datetime
from logs import errlog  # log
from web import (
    StaticUrls,
    Platform,
    AuthProcedure,
    AbstractSSOAuth,
)
from content import Lesson, Stream, Cut, Timestamp, Comment


class PanoptoDeliveryErrors(Enum):
    Success = 0
    SiteDisabled = 1
    LoginRequired = 2
    AccessRequired = 3
    PodcastRequired = 4
    InvalidUrl = 5
    InvalidDelivery = 6
    UnknownError = 7
    NotInAvailabilityWindow = 8
    InvalidFormatForSSL = 9
    InvalidPlaylist = 10
    InvalidPlaylistUrl = 11
    InvalidPlaylistNoSessions = 12
    HasPendingMergeOrCopyJobs = 13
    NetworkAccessDenied = 14
    Archived = 15


@dataclass
class PanoptoError(Exception):
    code: PanoptoDeliveryErrors = PanoptoDeliveryErrors.UnknownError


class ResponseParser(ABC):
    @abstractmethod
    def from_response(self, data: Dict):
        ...


@dataclass
class PanoptoAuth(AuthProcedure):
    domain: str
    sso_auth: AbstractSSOAuth
    LABEL = "Panopto"
    TEST_PATH = "/Panopto/Pages/Auth/CookieCheck.aspx"
    AUTH_PATH = "/Panopto/Pages/Auth/Login.aspx?instance=AAP-Univr&AllowBounce=true"  # (also handled via SAMLv2)
    HOME_PATH = "/Panopto/Pages/Home.aspx"

    RGX_TEST_JS = re.compile(r"isAuthenticated:[ \n]+?(?P<value>false|true)", flags=re.I | re.M)

    def is_authorized(self) -> Tuple[bool, Optional[str]]:
        with self.session.get(self.urljoin(self.TEST_PATH), strict200=False) as response:
            if response.is_redirect:
                return False, None
            auth = self.RGX_TEST_JS.search(response.text)
            return auth and auth["value"] == "true", None

    def authorize(self, redir: str = None) -> bool:
        if not redir:
            chk, redir = self.is_authorized()
            if chk:
                return True

        with self.session.get(self.urljoin(self.AUTH_PATH), strict200=False) as response:
            redir = response.next.url
        redir = self.sso_auth.login_portal(redir)

        rparts = ulp.urlparse(redir)
        assert self.domain in rparts.netloc and rparts.path == self.TEST_PATH
        with self.session.get(redir, strict200=False) as response:
            redir = response.next.url

        rparts = ulp.urlparse(redir)
        assert self.domain in rparts.netloc and rparts.path == self.HOME_PATH
        return self.is_authorized()[0]


class Panopto(Platform["Panopto.Urls"]):
    class Urls(StaticUrls):
        HOME_PAGE = "{}/Panopto/Pages/Home.aspx"
        LOGIN_PAGE = "{}/Panopto/Pages/Auth/Login.aspx"
        SERVICE_SESSIONS = "{}/Panopto/Services/Data.svc/GetSessions"
        API_FOLDERS = "{}/Panopto/Api/Folders"
        VIEWER_PAGE = "{}/Panopto/Pages/Viewer.aspx"
        DELIVERYINFO = "{}/Panopto/Pages/Viewer/DeliveryInfo.aspx"
        VIEW_RESULTS = "{}/Panopto/Pages/Viewer/Search/Results.aspx"

    # dynamic urls (unknown domains)
    # SOCIAL_URL_FMT = 'https://{}/Panopto/Podcast/Social/{}.mp4'
    THUMB_URL_FMT = "https://{}/sessions/{}/{}_et/thumbs/slide{}.jpg"
    SLIDE_URL_FMT = "https://{}/sessions/{}/{}_et/images/slide{}.jpg"

    RGX_DATE = re.compile(r"/Date\(([0-9]+)\)/")

    def __post_init__(self):
        super().__post_init__()
        self.auth = PanoptoAuth(session=self.session, domain=self.domain, sso_auth=self.sso_auth)

    def _request_list(self, params: Dict, url: str) -> List:  # GET & json list
        with self.session.get(
            url, params=params, headers=dict(accept='application/json')
        ) as response:
            try:
                data = response.json()
            except json.JSONDecodeError as err:
                errlog()
                errlog("Not a JSON!")
                raise err

        return data

    def _request_content(self, params: Dict, url: str, key: str) -> Dict:  # POST & json obj[key]
        with self.session.post(
            url, data=params, headers=dict(accept='application/json')
        ) as response:
            try:
                data = response.json()
            except json.JSONDecodeError as err:
                errlog()
                errlog("Not a JSON!")
                raise err

        errcode = data.get("ErrorCode")
        if errcode is not None and errcode != PanoptoDeliveryErrors.Success:
            # errlog("Panopto reports an error:")
            # with logger:
            # errlog(is_error, pointer=True)
            raise PanoptoError(PanoptoDeliveryErrors(errcode))
        info = data.get(key)
        if info is None:
            errlog(data)
            raise RuntimeError(f'Data does not contain "{key}" info!')
        return info

    def get_folders(self) -> List:
        query = dict(
            parentId="null",
            folderSet="1",
            includeMyFolder="false",
            includePersonalFolders="true",
            page="0",
            sort="Depth",
        )
        query["names[0]"] = "SessionCount"
        folders = self._request_list(query, self.urls.API_FOLDERS)

        return folders

    def get_folder_lessons_ids(self) -> List:
        params = dict(
            queryParameters=dict(
                bookmarked=False,
                startDate=None,
                endDate=None,
                getFolderData=False,  # subfolders
                includeArchived=True,
                includePlaylists=False,  # unsupported, unknown
                isSharedWithMe=False,
                isSubscriptionsPage=False,
                maxResults=10000,  # hopefully enough
                page=0,
                query=None,
                sortColumn=1,  # date?
                sortAscending=True,  # older first
            )
        )
        raw_lessons = self._request_content(params, self.urls.SERVICE_SESSIONS, "Results")

        # original lesson resides in "SessionID", but it could be deleted
        return list(map(lambda l: l["DeliveryID"], raw_lessons))

    def get_lesson_data(self, lesson_id: str) -> Lesson:
        params = dict(
            deliveryId=lesson_id,
            invocationId="",
            isLiveNotes="false",
            refreshAuthCookie="true",
            isActiveBroadcast="false",
            isEditing="false",
            isKollectiveAgentInstalled="false",
            isEmbed="false",
            responseType="json",
        )
        delivery = self._request_content(params, self.urls.DELIVERYINFO, "Delivery")

        delivery_domain = ulp.urlparse(delivery["Streams"][0]["StreamUrl"]).netloc
        if isinstance(delivery_domain, bytes):
            delivery_domain = delivery_domain.decode()
        session_id = delivery["SessionPublicID"]

        # ALERT Panopto uses BOTH Windows-epoch and Unix-epoch timestamps
        windowsdate = delivery["SessionStartTime"]
        if windowsdate is not None:
            windowsdate = win_to_unix_datetime(windowsdate).astimezone(None).replace(tzinfo=None)
        unixdate = None
        timestamps = delivery["Timestamps"]
        if timestamps:
            ts0 = timestamps[0]
            unixdate = self.RGX_DATE.match(ts0["CreationDateTime"])
            if unixdate:
                unixdate = float(unixdate[1]) / 1000 - ts0["Time"]
                unixdate = dt.datetime.utcfromtimestamp(unixdate).astimezone(None).replace(tzinfo=None)

        def nint(t):
            return int(t) if t is not None else None

        def ntime(t):
            return int(round(t, 3) * 1000)  # do not use floats (milliseconds)

        def new_segment(s):
            return Cut(
                start1=ntime(s["Start"]),
                end1=ntime(s["End"]),
                start2=ntime(s["RelativeStart"]),
            )

        def new_stream(s):
            st = Stream(
                url=s["StreamUrl"],  # doesn't need cookies
                tag=s["Tag"],
                segments=[new_segment(s) for s in (s["RelativeSegments"] or [])],
                start2=ntime(s["RelativeStart"]),
                end2=ntime(s["RelativeEnd"]),
            )
            return st

        def new_timestamp(t):
            return Timestamp(
                kind=t["EventTargetType"],  # 'ObjectVideo' == not a slide
                is_slide=t["EventTargetType"] != "ObjectVideo" and t["EventTargetType"] != "Primary",
                number=nint(t["ObjectSequenceNumber"]),  # valid if >0
                caption=t["Caption"],
                content=t["Data"],
                time=ntime(t["Time"]),
                thumb_url=self.THUMB_URL_FMT.format(
                    delivery_domain,
                    session_id,
                    t["ObjectPublicIdentifier"],
                    t["ObjectSequenceNumber"],
                ),  # doesn't need cookies
                slide_url=self.SLIDE_URL_FMT.format(
                    delivery_domain,
                    session_id,
                    t["ObjectPublicIdentifier"],
                    t["ObjectSequenceNumber"],
                ),  # doesn't need cookies Nor any identification header
            )

        lesson = Lesson(
            id=lesson_id,
            is_raw=False,
            name=delivery["SessionName"],
            authors=[a["DisplayName"] for a in delivery["Contributors"]],
            date=windowsdate or unixdate,
            duration=ntime(delivery["Duration"]),
            streams=[new_stream(s) for s in delivery["Streams"]],
            podcast_streams=[new_stream(s) for s in delivery["PodcastStreams"]],
            # don't expect timestamps to be sorted by sequence number,
            # you may find 1->2->0->3
            timestamps=[new_timestamp(t) for t in timestamps],
        )
        # lesson.comments = self.get_panopto_lesson_comments(lesson_id)

        return lesson

    def get_lesson_comments(self, lesson_id: str) -> List[Comment]:
        params = dict(
            id=lesson_id, type="comments", deliveryRelative="true", refreshAuthCookie="true", responseType="json"
        )
        events = self._request_content(params, self.urls.VIEW_RESULTS, "Events")

        def to_date(txt):
            if txt is None:
                return None
            m = self.RGX_DATE.match(txt)
            if m:
                return m[1]
            return None

        comments = [
            Comment(
                user=e["UserDisplayName"],
                time=e["Time"],
                text=e["Data"],
                creation=to_date(e["CreationDateTime"]),
            )
            for e in events
            if e["EventTargetType"] == "Comments"
        ]

        return comments
