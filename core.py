import sys
import os
import io
import mmap
import shutil
import re
import math
import itertools
import json
import subprocess
import inspect
import threading
import enum
from abc import ABC, abstractmethod
from datetime import datetime, timezone
from pathlib import Path
from hashlib import blake2b
from typing import (
    List,
    Dict,
    Tuple,
    Sequence,
    AnyStr,
    Iterable,
    Iterator,
    Mapping,
    Callable,
    IO,
    Optional,
    Union,
    Any,
    TypeVar
)

from logs import my_exception_hook, log, errlog, dump_args

T = TypeVar("T")


TOTAL_ERRORS = 0
INITIAL_CWD = os.getcwd()
LOGDIR = os.path.join(INITIAL_CWD, "error_logs")
DEBUG_INPUT = os.path.join(INITIAL_CWD, "debug_input.txt")


# region Text

RGX_FORMAT_NAMES = re.compile(r"{([^:]*).*?}")


def format_default(fmt, kwdata, default="") -> str:
    func = lambda m: m[0] if (m[1] in kwdata and kwdata[m[1]] is not None) else default
    fmt = RGX_FORMAT_NAMES.sub(func, fmt)
    return fmt.format(**kwdata)


def digest(data: bytes, *args, **kwargs) -> str:
    return blake2b(data, *args, **kwargs).hexdigest()

# endregion


# region Numbers

def nice_time(totalsec: float, display: bool = False, display_ms: bool = False) -> str:
    hours, remainder = divmod(totalsec, 3600)
    minutes, seconds = divmod(remainder, 60)
    if not display:
        return "{:02d}:{:02d}:{:06.3f}".format(int(hours), int(minutes), seconds)
    if hours == 0:
        if minutes == 0:
            if display_ms and seconds < 10:
                return f"{seconds:5.3f}s"
            return f"{int(seconds):d}s"
        return "{:02d}:{:02d}".format(int(minutes), int(seconds))
    return "{:02d}:{:02d}:{:02d}".format(int(hours), int(minutes), int(seconds))


def comp_ms_time(n):
    # module is used on minutes to remove hours without printing them
    if n is None:
        return "None"
    neg = n < 0
    s = abs(n) / 1000
    m, s = divmod(s, 60)
    return "{}{:02d}:{:02d}".format(('-' if neg else ''), int(m % 60), int(round(s + .5)))


BYTES_SCALES = ("", "K", "M", "G", "T")


def nice_bytes(totalbytes: int) -> str:
    degree = min(int(math.log(abs(totalbytes), 1024) if totalbytes != 0 else 0), 3)
    scaled = totalbytes / (1024**degree)
    if degree == 0:
        return f"{int(scaled):d}B"
    return f"{scaled:.2f}{BYTES_SCALES[degree]}iB"


EPOCH_UNIX = datetime(1970, 1, 1, tzinfo=timezone.utc)
EPOCH_WIN = datetime(1601, 1, 1, tzinfo=timezone.utc)
EPOCH_DELTA = EPOCH_UNIX - EPOCH_WIN


def win_to_unix_datetime(seconds: float) -> datetime:
    unix = seconds - EPOCH_DELTA.total_seconds()
    return datetime.utcfromtimestamp(unix)

# endregion


# region Platform

def ensure_directory(d: Path) -> None:
    if not d.is_dir():
        if d.is_symlink() and not d.exists():
            raise FileNotFoundError(f"This symlink targets a non-existing path: {d}")
        if d.exists():
            raise FileExistsError(f"This path is already taken by a file: {d}")
        d.mkdir()


def walk_no_recur_symlinks(top) -> Iterator[Tuple[AnyStr, List[AnyStr], List[AnyStr]]]:
    for path, dirs, files in os.walk(top, topdown=True, followlinks=True):
        apath = os.path.abspath(path)

        prune = list()
        for i, d in enumerate(dirs):
            d = os.path.join(path, d)
            if not os.path.islink(d):
                continue
            d = os.path.join(path, os.readlink(d))
            linkdest = os.path.abspath(d)
            # recursive symlink
            if apath.startswith(linkdest):
                # balance index
                # print(f"{d} is recursive ({i})")
                prune.append(i - len(prune))
        # print(dirs, prune)
        for p in prune:
            del dirs[p]

        yield apath, dirs, files


EasyProcessArgs = Sequence[Union[Tuple[Any], Tuple[Any, Optional[Any]]]]


def popen_easy(pargs: EasyProcessArgs, *args, **kwargs):
    pargs2 = []
    for t in pargs:
        if len(t) == 1:
            pargs2.append(str(t[0]))
        elif t[1] is not None:
            pargs2.append(str(t[0]))
            pargs2.append(str(t[1]))
    dump_args("POPEN", pargs2)
    return subprocess.Popen(pargs2, *args, **kwargs)


FFPROBE_FORMAT_STREAMS_ARGS = (
    ("ffprobe",),
    ("-show_format",),
    ("-show_streams",),
    ("-of", "json"),
)


def ffprobe_format_streams(finput, timeout: float = None):
    pargs2 = FFPROBE_FORMAT_STREAMS_ARGS + ((finput,),)
    proc = popen_easy(
        pargs2,
        stdin=subprocess.DEVNULL,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        bufsize=0,
        universal_newlines=True,
        shell=False,
    )
    stdout: IO = proc.stdout
    proc.wait(timeout)
    txt = stdout.read()
    # errlog(txt)
    if proc.returncode != 0:
        raise subprocess.SubprocessError(f"Process returned {proc.returncode}.\n\n{proc.stderr.read()}")
    return json.loads(txt)


def ebml_get_len(f: io.IOBase) -> int:
    # http://matroska-org.github.io/libebml/specs.html
    data = bytearray(f.read(1))
    num = data[0]
    mask = 0xFF
    for i in range(0, 8):
        shift = 7 - i
        mask >>= 1
        if (num & 2**shift) >> shift:
            if i > 0:
                data += f.read(i)
            break
    else:
        raise RuntimeError("Invalid length.")
    data[0] &= mask
    return int.from_bytes(data, "big")


def get_mkv_title(path: str) -> Optional[str]:
    if os.path.getsize(path) <= 0:
        return None
    with open(path, "rb") as fin:
        with mmap.mmap(fin.fileno(), 0, access=mmap.ACCESS_READ) as mkv:
            mkv: mmap.mmap

            def err():
                raise RuntimeError("Invalid mkv.")

            def try_match(b: bytes):
                return mkv.read(len(b)) == b

            def match(b: bytes):
                if not try_match(b):
                    err()

            def length():
                return ebml_get_len(mkv)  # noqa

            def ignore():
                mkv.seek(length(), io.SEEK_CUR)

            if not try_match(b"\x1A\x45\xDF\xA3"):  # ebml header
                return None
            # mkv.seek(4*4 + 1, io.SEEK_CUR)
            # match(b'\x42\x82\x88matroska')
            # mkv.seek(4*2, io.SEEK_CUR)
            ignore()
            match(b"\x18\x53\x80\x67")  # segment
            length()
            match(b"\x11\x4D\x9B\x74")  # seek head
            ignore()
            match(b"\xEC")  # void
            ignore()
            match(b"\x15\x49\xA9\x66")  # info
            ilen = length()

            # title
            tid = b"\x7b\xa9"
            cur = mkv.tell()
            tpos = mkv.find(tid, cur, cur + ilen)
            if tpos < cur:
                err()
            mkv.seek(tpos + len(tid), io.SEEK_SET)
            title = mkv.read(length())
            return title.decode()


BackgroundMultiprocesses: List = []


def sigterm_back_proc():
    for proc in BackgroundMultiprocesses:
        proc.terminate()
    BackgroundMultiprocesses.clear()


def criticalsection(func: Callable):
    lock = threading.Lock()
    func.thread_lock = lock
    def wrapper(*a, **kw):
        with lock:
            return func(*a, **kw)
    return wrapper


def missing_software(*softwares) -> List[str]:
    return list(filter(lambda s: shutil.which(s) is None, softwares))


def set_file_times(f, date):
    if date is None:
        date = datetime.now()
    if isinstance(date, datetime):
        date = date.timestamp()
    elif date is not float:
        raise ValueError("File time must be a datetime or float.")
    os.utime(f, times=(date, date))


def get_file_time(f):
    return datetime.fromtimestamp(os.path.getmtime(f))


def simple_shred(filename: str):
    size0 = os.path.getsize(filename)
    for _1 in range(1, 5 + 1):
        size1 = size0
        with open(filename, mode="wb") as f:
            while size1 > 0:
                block = min(size1, 4096)
                f.write(os.urandom(block))
                size1 -= block
    os.remove(filename)


r""" http://stackoverflow.com/questions/1976007/ddg#31976060
< (less than)
> (greater than)
: (colon - sometimes works, but is actually NTFS Alternate Data Streams)
" (double quote)
/ (forward slash)
\ (backslash)
| (vertical bar or pipe)
? (question mark)
* (asterisk)
"""
WINDOWS_PATH_TRANS = str.maketrans('\t<>:"/\\&', " ();'--e", "|?*\x00")


def windows_compatible_path(pth: str) -> str:
    return pth.translate(WINDOWS_PATH_TRANS)


def now_path() -> str:
    return datetime.now().isoformat().replace(':', '-')

# endregion


# region Language

def copyupdate(a: Dict, b: Dict) -> Dict:
    a = a.copy()
    a.update(b)
    return a


def _recur_get(d, k):
    if isinstance(d, dict):
        return d.get(k)
    if 0 <= k < len(d):
        return d[k]
    return None


def recur_get(a, *path, default=None) -> Any:
    if path:
        if a is None:
            return default
        k = path[0]
        a = _recur_get(a, k)
        return recur_get(a, *path[1:], default=default)
    return a


def len2(it: Iterable, stopat: int) -> int:
    l = 0
    for _ in it:
        l += 1
        if l >= stopat:
            return l
    return l


def comparison_chain(seq: List[T]) -> Iterable[Tuple[T, T]]:
    if len2(seq, 2) < 2:
        return
    not_first = False
    old: T = None
    for e in seq:
        if not_first:
            yield old, e
        not_first = True
        old = e


def group_by(seq: List, kfunc: Callable):
    return itertools.groupby(sorted(seq, key=kfunc), kfunc)


def coalesce(d: Mapping, *keys, default=None):
    if not keys:
        return default
    if keys[0] in d:
        return d[keys[0]]
    return coalesce(d, *keys[1:], default=default)


def linear_maps(args, *mappings):
    for m in mappings:
        args = m(args)
    return args


def take_unique_with_order(it: Iterable) -> List:
    return list(dict.fromkeys(it))


def thisarg(func):
    def wrapped(*args, **kwargs):
        return func(wrapped, *args, **kwargs)

    return wrapped


def pub_class_priv_attr(obj, name):
    c = obj.__class__
    pre = ("_" + c.__name__) if name.startswith("__") else ""
    return c.__dict__[pre + name]


def anon_object(**kwargs):
    # https://stackoverflow.com/a/24448351
    # create a new anonymous class derived from object (by default)
    # with kwargs dictionary, and instantiate it
    return type("", (), kwargs)()


def last_name() -> str:
    prev_scope = inspect.currentframe().f_back
    a = prev_scope.f_locals.get("__annotations__")
    if not a:
        raise AttributeError("At least one annotation must be defined.")
    return next(reversed(a.keys()))


def makenames(cls, ref=None):
    if not ref:
        ref = cls
    try:
        a = ref.__annotations__
    except AttributeError:
        return cls
    for k in a.keys():
        setattr(cls, "K_" + str.upper(k), k)
    return cls


def makenone(cls, ref=None, name=None):
    if not ref:
        ref = cls
    try:
        a = ref.__annotations__
    except AttributeError:
        return cls
    for k in (name,) if (name is not None) else a.keys():
        if not hasattr(cls, k):
            setattr(cls, k, None)
    return cls


def exitnow(msg: str = None):
    if msg:
        errlog(msg + ".")
    errlog("It's pointless to continue, closing...")
    sys.exit(1)


def makesure(func, msg: str = None, cls: Callable = RuntimeError, exit_: bool = False):
    if not func():
        if exit_:
            exitnow(msg)
            return
        if msg:
            msg += "."
            raise cls(msg)
        raise cls()


def error_to_file():
    global TOTAL_ERRORS
    os.makedirs(LOGDIR, exist_ok=True)
    exc_type, ex, trace = sys.exc_info()
    name = ex.__class__.__name__
    fname = f"{now_path()}.{name}.ans"
    dest = os.path.join(LOGDIR, fname)
    with open(dest, "w", encoding="utf-8") as fout:
        my_exception_hook(exc_type, ex, trace, file=fout)
    errlog(name, end="")
    if hasattr(ex, "message"):
        errlog(f": {ex.message}", end="", pad=False)
    errlog(f" :(   please report me: {fname}\n", pad=False)
    TOTAL_ERRORS += 1

# endregion


class SimpleEnterExit:
    def __init__(self, enter, exit_):
        self.enter = enter
        self.exit = exit_

    def __enter__(self):
        self.enter()
        return self

    def __exit__(self, exc_type, exc_value, traceback_):
        self.exit()


class Loadable(ABC):
    def __enter__(self):
        self.load()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.unload()

    @abstractmethod
    def load(self):
        ...

    @abstractmethod
    def unload(self):
        ...


# noinspection PyMethodMayBeStatic
class ErrorLogger:
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        log(msg)


class StrEnum(str, enum.Enum):
    def __new__(cls, value, *args, **kwargs):
        if not isinstance(value, (str, enum.auto)):
            raise TypeError(f"Values of StrEnums must be strings: {value!r} is a {type(value)}")
        return super().__new__(cls, value, *args, **kwargs)  # noqa

    def __str__(self):
        return str(self.value)

    @staticmethod
    def _generate_next_value_(name: str, *_):  # used by auto()
        return name



#region Debugging

def is_debugging():
    gettrace = getattr(sys, "gettrace", None)
    if gettrace is None:
        return False
    if gettrace():
        return True
    return False

if is_debugging() and Path(DEBUG_INPUT).is_file():
    import atexit
    sys.stdin = open(DEBUG_INPUT, "r")
    atexit.register(lambda f: f.close(), sys.stdin)

#endregion
