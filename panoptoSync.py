#!/usr/bin/env python3

"""
Main PanoptoSync module.
"""

import sys
import urllib3.exceptions
from pathlib import Path
from typing import Optional

import core
from logs import logger, errlog
import args as module_args


ARGS: module_args.ArgumentsType


def main(argv) -> Optional[int]:
    from core import sigterm_back_proc
    from input import Credentials

    global ARGS

    parser = module_args.make_command_parser()
    if len(argv) == 1:
        parser.print_help(sys.stderr)
        return 1
    ARGS = parser.parse_args(argv[1:])
    logger.verbosity = ARGS.verb_flags
    ARGS.id_path = Path(ARGS.id_path)

    actions = {
        module_args.ACT_GENID: lambda: Credentials.generate(ARGS.id_path),
        module_args.ACT_SHOWID: lambda: Credentials.show(ARGS.id_path),
        module_args.ACT_SYNC: syncronize,
    }

    def check_errors():
        if core.TOTAL_ERRORS > 0:
            errlog()
            errlog(f"Warning: {core.TOTAL_ERRORS} errors - please report them.")
            errlog(f'(check latest files in "{core.LOGDIR}" folder)')

    def close(msg=None):
        sigterm_back_proc()
        if msg:
            errlog()
            errlog(msg)
        check_errors()

    try:
        #raise Exception("test")
        return actions[ARGS.action]()
    except EOFError:
        close("EOF reached.")
        return 1
    except (KeyboardInterrupt, urllib3.exceptions.MaxRetryError):
        close("Interrupt received.")
        return 1
    except BaseException as unk:
        core.error_to_file()
        close()
        raise unk


def syncronize() -> Optional[int]:
    import sync

    if not ARGS.only_rename and sync.SYNC_MISSING_SOFTWARE:
        errlog(
            "Unable to find some softwares.",
            "Please install and make them visible from the system path.",
            sep="\n",
        )
        with logger:
            errlog(", ".join(sync.SYNC_MISSING_SOFTWARE))
        return 1

    # arguments of sync parser
    ARGS.cookies_path = Path(ARGS.cookies_path)
    ARGS.sync_dir = Path(ARGS.sync_dir)
    if ARGS.only_rename:
        ARGS.no_search = True
    if ARGS.only_meta:
        ARGS.force_meta = True

    sync.ARGS = ARGS
    with sync.Synchronizer() as s:
        if not s.run():
            return 1


if __name__ == "__main__":
    sys.exit(main(sys.argv) or 0)
