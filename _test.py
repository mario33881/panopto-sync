#!/usr/bin/env python3

#import time
import base64
import os.path
import unittest
import random as rnd
import datetime as dt
import atexit
from pathlib import Path
from datetime import datetime
from typing import Optional

os.chdir("research/indev")

from logs import Verb, logger
from core import ensure_directory, makesure, comp_ms_time
from extract_date import MONTHS, DAYS, find_known_date_format
#from progress_bar import CustomMixedBar
from web import SessionEx
from input import Credentials
from content import Lesson, Stream, Cut
from univr import UniVRBot
from download import LessonDownloader

logger.verbosity.extend((Verb.network, ))
skip_all_ok = "Tested: all working"
skip_ok = "Tested: working"

dummy_req = SessionEx()
bot: Optional[UniVRBot] = None
cred: Optional[Credentials] = None


def ask_credentials():
    global cred
    if not cred:
        idpath = Path("default.id")
        # cookiespath = Path("default.cookies")
        cred, encpsw = Credentials.read(idpath)


def load_bot():
    global bot
    if not bot:
        ask_credentials()
        # cookies = read_cookies(cookiespath, encpsw)
        bot = UniVRBot(cred)
        bot.load()

def unload_bot():
    global bot
    if bot:
        bot.unload()

atexit.register(unload_bot)


#@unittest.skip(skip_all_ok)
class TestGaps(unittest.TestCase):
    #@unittest.skip(skip_ok)
    def test_1_stream_gaps1(self):
        # real case: all aligned (pointless)
        segm = [
            Cut(start2=83000, start1=83000, end1=812000),
            Cut(start2=818000, start1=818000, end1=2419000),
            Cut(start2=2429000, start1=2429000, end1=3683000),
            Cut(start2=3713000, start1=3713000, end1=3993000),
            Cut(start2=4925000, start1=4925000, end1=5784000),
            Cut(start2=5820000, start1=5820000, end1=6129000),
            Cut(start2=6135000, start1=6135000, end1=8499000),
            Cut(start2=8598000, start1=8598000, end1=8598112),
        ]
        stre = Stream.__new__(Stream)
        stre.segments = segm
        stre.url = 'none'
        stre.tag = 'OBJECT'
        stre.__post_init__()
        lesson = Lesson.__new__(Lesson)
        lesson.duration = 8598112
        lesson.streams = [stre]
        lesson.filename = "out.mkv"
        lesson.timestamps = []
        lesson.__post_init__()
        ld = LessonDownloader(lesson, Path("whatever"), dummy_req)
        print()
        print("av_sync:", comp_ms_time(stre.all_segm_aligned))
        for p in ld._get_final_segments2(stre):
            print(repr(p))

    #@unittest.skip(skip_ok)
    def test_1_stream_gaps2(self):
        # real case: smaller fillers
        segm = [
            Cut(start2=212037, start1=359000, end1=2665000),
            Cut(start2=2530037, start1=2677000, end1=3443000),
            Cut(start2=3301037, start1=3448000, end1=5803000),
        ]
        stre = Stream.__new__(Stream)
        stre.segments = segm
        stre.url = 'none'
        stre.tag = 'OBJECT'
        stre.__post_init__()
        lesson = Lesson.__new__(Lesson)
        lesson.duration = 6161773
        lesson.streams = [stre]
        lesson.filename = "out2.mkv"
        lesson.timestamps = []
        lesson.__post_init__()
        ld = LessonDownloader(lesson, Path("whatever"), dummy_req)
        print()
        print("av_sync:", comp_ms_time(stre.all_segm_aligned))
        for p in ld._get_final_segments2(stre):
            print(repr(p))

    #unittest.skip(skip_ok)
    def test_1_stream_gaps3(self):
        # real case: small delay + cut
        segm = [
            Cut(start2=109, start1=0, end1=30875),
            Cut(start2=30984, start1=51865, end1=3196765),
        ]
        stre = Stream.__new__(Stream)
        stre.url = 'none'
        stre.tag = 'SCREEN'
        stre.segments = segm
        stre.__post_init__()
        lesson = Lesson.__new__(Lesson)
        lesson.duration = 3175884
        lesson.streams = [stre]
        lesson.filename = "out3.mkv"
        lesson.timestamps = []
        lesson.__post_init__()
        ld = LessonDownloader(lesson, Path("whatever"), dummy_req)
        print()
        print("av_sync:", comp_ms_time(stre.all_segm_aligned))
        for p in ld._get_final_segments2(stre):
            print(repr(p))


@unittest.skip(skip_all_ok)
class TestDates(unittest.TestCase):
    @unittest.skip(skip_ok)
    def test_2_known_date_format1(self):
        print()
        for _1 in range(1, 100):
            txt = " ".join(
                (
                    rnd.choice(rnd.choice(DAYS))[:3] + ",",
                    rnd.choice(rnd.choice(MONTHS)[:3]),
                    str(rnd.randint(1, 28)),
                    "2020",
                    "at",
                    rnd.choice((".", ":", ";")).join((str(rnd.randint(1, 12)), str(rnd.randint(0, 59)))),
                    rnd.choice(("AM", "PM")),
                )
            )
            num, match, *t = find_known_date_format(txt)
            print(txt)
            print(num, *t)
            dt.datetime(*t)

    @unittest.skip(skip_ok)
    def test_2_known_date_format2(self):
        print()
        for _1 in range(1, 100):
            txt = " ".join(
                (
                    rnd.choice(rnd.choice(DAYS))[:3],
                    str(rnd.randint(1, 28)),
                    rnd.choice(rnd.choice(MONTHS))[:3],
                    "2020",
                    "at",
                    ".".join(
                        (
                            str(rnd.randint(0, 23)),
                            str(rnd.randint(0, 59)),
                            str(rnd.randint(0, 59)),
                        )
                    ),
                )
            )
            num, match, *t = find_known_date_format(txt)
            dt.datetime(*t)

    @unittest.skip(skip_ok)
    def test_2_known_date_format5(self):
        print()
        for _1 in range(1, 100):
            txt = " ".join(
                (
                    str(rnd.randint(1, 28)),
                    rnd.choice(rnd.choice(MONTHS))[:3],
                    "2020",
                )
            )
            num, match, *t = find_known_date_format(txt)
            t[3] = 0
            t[4] = 0
            # print(num, *t)
            dt.datetime(*t)

    @unittest.skip(skip_ok)
    def test_3_other_date_formats(self):
        titles_date = (
            "IMPRENDITORIA_Testimonianza_T2i_M.Brunelli-A.Previato_07.05.20_Zoom Meeting",
            "IMPRENDITORIA_Testimone 7 aprile 20_Luca Guarnieri_LIAISON OFFICE UNIVR-presentazione",
            "Lezione_14Martedì_qualcosa",  # -> 14/??
        )
        titles_nodate = (
            "Lezione 7.2 Sistemi a tempo continuo. Risposta libera",
            "Lezione_14_Venerdì_12_qualcosa",
        )
        for txt in titles_date:
            print(txt)
            num, match, *t = find_known_date_format(txt)
            print(num, *t)
        for txt in titles_nodate:
            print(txt)
            ret = find_known_date_format(txt)
            print(ret)
            makesure(lambda: ret is None, "Must be None", TypeError)

    # @unittest.skip(skip_ok)
    def test_4_full_name_date_fix(self):
        titles = (
            "IMPRENDITORIA_Testimone 7 aprile 20_Luca Guarnieri_LIAISON OFFICE UNIVR-presentazione",
            "Gio23.04.2020(IV) - Lab4 - slides14-19 durata26'58''",
            "Wed, Mar 11 2020 at 1:54 PM",
            "Lez whatever (II; Lun 30/03/20)",
            "Lez del 14/2//20",
            "Lez del 14 2 20",
            "Crittografia 2 Integrità 1",
            "Compilatori 31 Marzo",
            "Analisi Matematica 1 - 2020-11-19",
        )
        lesson = Lesson.__new__(Lesson)
        lesson.date = datetime.now()
        for txt in titles:
            print(txt)
            lesson.name = txt
            lesson.assign_better_date_and_name()
            print("fixed:", lesson.name_fixed)
            print("date_name:", lesson.date_name)
            print("date_better:", lesson.date_better)
            print()


@unittest.skip(skip_all_ok)
class TestBot(unittest.TestCase):
    def setUp(self):  # noqa
        load_bot()

    @unittest.skip(skip_ok)
    def test_1_get_panopto_ids(self):
        for lesson in bot.get_panopto_ids_from_course_id(2877):
            print(">", lesson)

    @unittest.skip(skip_ok)
    def test_2_get_panopto_lesson_data(self):
        data = bot.panopto.get_lesson_data("c0895c0f-b49b-4f60-ad8d-ab7900eb1499")
        print(data)


#@unittest.skip(skip_all_ok)
class TestMain(unittest.TestCase):
    def setUp(self):
        load_bot()

        self.testdir = Path("lessons_test")
        ensure_directory(self.testdir)

    @unittest.skip(skip_ok)
    def test_1_get_panopto_lesson_data1(self):
        data = bot.panopto.get_lesson_data("3fd372be-60a5-4636-8c7c-ab7600b24d3b")
        print(data.__dict__)

    @unittest.skip(skip_ok)
    def test_1_get_panopto_lesson_data2(self):
        data = bot.panopto.get_lesson_data("3bebb456-697c-4fe9-8659-ab9600da2e38")
        print(data.__dict__)

    @unittest.skip(skip_ok)
    def test_1_get_panopto_lesson_data3_slides(self):
        data = bot.panopto.get_lesson_data("f6bb7d26-164d-4ff2-aeab-ab7b008c15c9")
        ts21 = list(filter(lambda ts: ts.is_slide and ts.number == 21, data.timestamps))[0]
        with dummy_req.get(ts21.slide_url) as resp:
            print(base64.b64encode(resp.content).decode())  # print raw data as base64 (bytes -> bytes -> str)

    @unittest.skip(skip_ok)
    def test_2_download_lesson1(self):  # dv
        data = bot.panopto.get_lesson_data("db271bac-ca01-43e5-923e-aea000b03f25")
        data.filename = "test2.1.mkv"
        with LessonDownloader(data, self.testdir, dummy_req) as d:
            d.run(True)

    @unittest.skip(skip_ok)
    def test_2_download_lesson2(self):  # common lesson
        data = bot.panopto.get_lesson_data("3bebb456-697c-4fe9-8659-ab9600da2e38")
        data.filename = "test2.2.mkv"
        with LessonDownloader(data, self.testdir, dummy_req) as d:
            d.run(True)

    @unittest.skip(skip_ok)
    def test_2_download_lesson3(self):  # audio+slides
        data = bot.panopto.get_lesson_data("c0895c0f-b49b-4f60-ad8d-ab7900eb1499")
        data.filename = "test2.3.mkv"
        with LessonDownloader(data, self.testdir, dummy_req) as d:
            d.run(True)

    @unittest.skip(skip_ok)
    def test_2_download_lesson4(self):  # audio+slides
        data = bot.panopto.get_lesson_data("f6bb7d26-164d-4ff2-aeab-ab7b008c15c9")
        data.filename = "test2.4.mkv"
        with LessonDownloader(data, self.testdir, dummy_req) as d:
            d.run(True)

    @unittest.skip(skip_ok)
    def test_2_download_lesson5(self):  # weird offsets
        data = bot.panopto.get_lesson_data("280da7f8-54d8-45df-80f0-ab8e00ea8d43")
        data.filename = "test2.5.mkv"
        with LessonDownloader(data, self.testdir, dummy_req) as d:
            d.run(True)

    @unittest.skip(skip_ok)
    def test_2_download_lesson6(self):  # 3 streams, offsets
        data = bot.panopto.get_lesson_data("341a8d03-7dde-442d-93fb-ab98008f63e5")
        data.filename = "test2.6.mkv"
        with LessonDownloader(data, self.testdir, dummy_req) as d:
            d.run(True)

    @unittest.skip(skip_ok)
    def test_2_download_byterange1(self):
        data = bot.panopto.get_lesson_data("be120ace-d676-4b24-8651-ae8a007ebeff")
        data.filename = "test2.byterange1.mkv"
        with LessonDownloader(data, self.testdir, dummy_req) as d:
            d.run(True)

    #@unittest.skip(skip_ok)
    def test_2_download_cuts1(self):
        # sometimes gaps are present, but fillers are Also present in the raw video
        # 0b39a7e1-3879-4b8e-9c23-ac4800c32a95
        # 6ae0c5dd-5d61-4f19-8b6e-ad2d00ae288c  ~13:35
        data = bot.panopto.get_lesson_data("557026d9-b1c9-4f3d-af6a-acfc00c4f45d")
        data.filename = "test2.cuts1.mkv"
        with LessonDownloader(data, self.testdir, dummy_req) as d:
            #d._DEBUG_ONLY_SEGMENTS = True
            d.run(True)

    #@unittest.skip(skip_ok)
    def test_2_download_cuts2(self):
        data = bot.panopto.get_lesson_data("43d899e8-360f-40ad-ae79-ae9f007ebc9a")
        data.filename = "test2.cuts2.mkv"
        with LessonDownloader(data, self.testdir, dummy_req) as d:
            #d._DEBUG_ONLY_SEGMENTS = True
            d.run(True)

    #@unittest.skip(skip_ok)
    def test_2_download_screen_multistream(self):
        data = bot.panopto.get_lesson_data("a59ea9a6-3ce2-4e9c-b89a-ae1b01133e7d")
        data.filename = "test2.multiscreen.mkv"
        with LessonDownloader(data, self.testdir, dummy_req) as d:
            #d._DEBUG_ONLY_SEGMENTS = True
            d.run(True)

    #@unittest.skip(skip_ok)
    def test_2_download_bug(self):
        data = bot.panopto.get_lesson_data("a70bdaff-82c0-4b92-86b0-afea011681f6")
        data.filename = "test2.bug.mkv"
        with LessonDownloader(data, self.testdir, dummy_req) as d:
            #d._DEBUG_ONLY_SEGMENTS = True
            d.run(True)


if __name__ == "__main__":
    suite = unittest.TestSuite()
    for test in (TestGaps, TestDates, TestBot, TestMain):
        suite.addTest(unittest.makeSuite(test))
    runner = unittest.TextTestRunner(verbosity=2, failfast=True)
    runner.run(suite)
