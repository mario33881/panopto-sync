"""
Module defining an AES cipher class that can encrypt/decrypt data (of string or bytes).

https://stackoverflow.com/a/21928790 + modifications for correctness check
"""

import base64
import hashlib
from Cryptodome import Random
from Cryptodome.Cipher import AES


class AESCipher:
    """
    Encrypts/decrypts bytes and strings.
    :param key: secret key passed to the cipher
    """

    _rnd = Random.new()  # random number generator

    def __init__(self, key: bytes):
        self.block_size = AES.block_size
        self.key = hashlib.sha256(key).digest()

    def encrypt(self, raw: bytes) -> bytes:
        raw = self._pad(raw)
        check = hashlib.sha256(raw).digest()
        iv = AESCipher._rnd.read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        data = cipher.encrypt(raw)
        return base64.b64encode(check + iv + data)

    def decrypt(self, enc: bytes) -> bytes:
        enc = base64.b64decode(enc)
        check = enc[:32]
        iv = enc[32 : 32 + self.block_size]
        # print(len(check), len(iv), len(enc))
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        dec = cipher.decrypt(enc[32 + self.block_size :])
        if check != hashlib.sha256(dec).digest():
            raise ValueError("Invalid key.")
        return self._unpad(dec)

    def encrypt_text(self, text: str) -> str:
        return self.encrypt(text.encode()).decode()

    def decrypt_text(self, enc: str) -> str:
        return self.decrypt(enc.encode()).decode()

    def _pad(self, s: bytes) -> bytes:
        """
        Adds padding to ``s`` so that it has the same length as ``self.block_size``.
        """
        psize = self.block_size - len(s) % self.block_size
        return s + psize * bytes([psize])

    @staticmethod
    def _unpad(s: bytes) -> bytes:
        """
        Removes padding from ``s``.
        """
        return s[: -ord(s[len(s) - 1 :])]
